                           !               2020.3.0f1 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙    Ŕ           1  1  ˙˙˙˙                Ţ                        j  ˙˙˙˙                \     ˙˙˙˙                H r   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙               1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                      Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                       \       ŕyŻ     `       ,                                                                                                                                            ŕyŻ                                                                                    HandJointsManager     // Copyright (c) Microsoft Corporation.
// Licensed under the MIT License.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace Microsoft.MixedReality.OpenXR.Samples
{
    public class HandJointsManager : MonoBehaviour
    {
        private enum HandJointSource
        {
            FeatureUsage,
            OpenXRExtension,
        }

        [SerializeField, Tooltip("The hand joint source for the left hand.")]
        private HandJointSource leftHandJointSource = HandJointSource.FeatureUsage;

        [SerializeField, Tooltip("The hand joint source for the right hand.")]
        private HandJointSource rightHandJointSource = HandJointSource.FeatureUsage;

        [SerializeField, Tooltip("The prefab to use for rendering hand joints in the scene. (optional)")]
        private GameObject handJointPrefab = null;

        private Hand leftHand = null;
        private Hand rightHand = null;

        private static readonly HandJointLocation[] HandJointLocations = new HandJointLocation[HandTracker.JointCount];

        private void OnEnable()
        {
            Application.onBeforeRender += Application_onBeforeRender;
        }

        private void OnDisable()
        {
            Application.onBeforeRender -= Application_onBeforeRender;
            leftHand?.DisableHandJoints();
            rightHand?.DisableHandJoints();
        }

        private void Application_onBeforeRender()
        {
            if (leftHandJointSource == HandJointSource.OpenXRExtension)
            {
                UpdateHandJointsUsingOpenXRExtension(HandTracker.Left, leftHand, FrameTime.OnBeforeRender);
            }
            if (rightHandJointSource == HandJointSource.OpenXRExtension)
            {
                UpdateHandJointsUsingOpenXRExtension(HandTracker.Right, rightHand, FrameTime.OnBeforeRender);
            }
        }

        private void Start()
        {
            leftHand = new Hand(handJointPrefab);
            rightHand = new Hand(handJointPrefab);
        }

        private void Update()
        {
            if (leftHandJointSource == HandJointSource.OpenXRExtension)
            {
                UpdateHandJointsUsingOpenXRExtension(HandTracker.Left, leftHand, FrameTime.OnUpdate);
            }
            else
            {
                UpdateHandJointsUsingFeatureUsage(InputDeviceCharacteristics.Left, leftHand);
            }

            if (rightHandJointSource == HandJointSource.OpenXRExtension)
            {
                UpdateHandJointsUsingOpenXRExtension(HandTracker.Right, rightHand, FrameTime.OnUpdate);
            }
            else
            {
                UpdateHandJointsUsingFeatureUsage(InputDeviceCharacteristics.Right, rightHand);
            }
        }

        private static void UpdateHandJointsUsingFeatureUsage(InputDeviceCharacteristics flag, Hand hand)
        {
            List<InputDevice> inputDeviceList = new List<InputDevice>();
            InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.HandTracking, inputDeviceList);
            UnityEngine.XR.Hand xrHand = default;
            foreach (InputDevice device in inputDeviceList)
            {
                if (device.characteristics.HasFlag(flag) && device.TryGetFeatureValue(CommonUsages.handData, out xrHand))
                {
                    break;
                }
            }

            if (xrHand != default)
            {
                hand?.UpdateHandJoints(xrHand);
            }
            else
            {
                // If we get here, we didn't successfully update hand joints for any tracked input device
                hand?.DisableHandJoints();
            }
        }

        private static void UpdateHandJointsUsingOpenXRExtension(HandTracker handTracker, Hand hand, FrameTime frameTime)
        {
            if (handTracker.TryLocateHandJoints(frameTime, HandJointLocations))
            {
                hand?.UpdateHandJoints(HandJointLocations);
            }
            else
            {
                hand?.DisableHandJoints();
            }
        }
    }
}
                        HandJointsManager   %   Microsoft.MixedReality.OpenXR.Samples   