﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.MixedReality.OpenXR.Samples.PlacementOnPlanes::Awake()
extern void PlacementOnPlanes_Awake_mB0BBA095199EECE89A888EFCEC16072AFB892927 (void);
// 0x00000002 System.Void Microsoft.MixedReality.OpenXR.Samples.PlacementOnPlanes::Update()
extern void PlacementOnPlanes_Update_m6504827D87E63EF252E20F7310F720D81193838C (void);
// 0x00000003 System.Void Microsoft.MixedReality.OpenXR.Samples.PlacementOnPlanes::Raycast(UnityEngine.Pose)
extern void PlacementOnPlanes_Raycast_m9C233EFE71F086281FA56D5A7F2D22EC2A12AD40 (void);
// 0x00000004 System.Void Microsoft.MixedReality.OpenXR.Samples.PlacementOnPlanes::.ctor()
extern void PlacementOnPlanes__ctor_m10D36C7ACD078CDD559BFA5A41EEFC6D72EE04AE (void);
// 0x00000005 System.Void Microsoft.MixedReality.OpenXR.Samples.PlacementOnPlanes::.cctor()
extern void PlacementOnPlanes__cctor_m3A5C9F7DA35B798F3FF039C2F94452B42A580C08 (void);
// 0x00000006 System.Void Microsoft.MixedReality.OpenXR.Samples.PlanesSample::Awake()
extern void PlanesSample_Awake_m0E1F431355DFB802CC3A5F2EE515B57633136A55 (void);
// 0x00000007 System.Void Microsoft.MixedReality.OpenXR.Samples.PlanesSample::Update()
extern void PlanesSample_Update_m3A0D42AD33279E4E2A6A6ACE8AD3E8893F6FD6F0 (void);
// 0x00000008 System.Void Microsoft.MixedReality.OpenXR.Samples.PlanesSample::SetPlaneDetectionMode(System.Int32)
extern void PlanesSample_SetPlaneDetectionMode_mECDCD59EFBB27C7D6394F3C53226FBBA5960C079 (void);
// 0x00000009 System.Void Microsoft.MixedReality.OpenXR.Samples.PlanesSample::.ctor()
extern void PlanesSample__ctor_m20E158C56F4646532BC5D21849CB966EC91B79B5 (void);
// 0x0000000A System.Void Microsoft.MixedReality.OpenXR.Samples.SamplePlane::Start()
extern void SamplePlane_Start_m5C3943BB797DA0BB66BBC3B85A05E24723B3405A (void);
// 0x0000000B System.Void Microsoft.MixedReality.OpenXR.Samples.SamplePlane::.ctor()
extern void SamplePlane__ctor_m085E87FBD8E5F4C775FC159DA694ED90AD6627F1 (void);
static Il2CppMethodPointer s_methodPointers[11] = 
{
	PlacementOnPlanes_Awake_mB0BBA095199EECE89A888EFCEC16072AFB892927,
	PlacementOnPlanes_Update_m6504827D87E63EF252E20F7310F720D81193838C,
	PlacementOnPlanes_Raycast_m9C233EFE71F086281FA56D5A7F2D22EC2A12AD40,
	PlacementOnPlanes__ctor_m10D36C7ACD078CDD559BFA5A41EEFC6D72EE04AE,
	PlacementOnPlanes__cctor_m3A5C9F7DA35B798F3FF039C2F94452B42A580C08,
	PlanesSample_Awake_m0E1F431355DFB802CC3A5F2EE515B57633136A55,
	PlanesSample_Update_m3A0D42AD33279E4E2A6A6ACE8AD3E8893F6FD6F0,
	PlanesSample_SetPlaneDetectionMode_mECDCD59EFBB27C7D6394F3C53226FBBA5960C079,
	PlanesSample__ctor_m20E158C56F4646532BC5D21849CB966EC91B79B5,
	SamplePlane_Start_m5C3943BB797DA0BB66BBC3B85A05E24723B3405A,
	SamplePlane__ctor_m085E87FBD8E5F4C775FC159DA694ED90AD6627F1,
};
static const int32_t s_InvokerIndices[11] = 
{
	4910,
	4910,
	3995,
	4910,
	7190,
	4910,
	4910,
	3948,
	4910,
	4910,
	4910,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	11,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
