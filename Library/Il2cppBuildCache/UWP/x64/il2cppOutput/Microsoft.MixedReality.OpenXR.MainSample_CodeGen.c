﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void RuntimeInfo::Start()
extern void RuntimeInfo_Start_m1D3D82223508B59B3D9CABC900EEFBF84AF839E6 (void);
// 0x00000002 System.Void RuntimeInfo::.ctor()
extern void RuntimeInfo__ctor_m2E2554F3B318E8B3BF85A4B412CF9B11B9AF4BE4 (void);
// 0x00000003 System.Void Microsoft.MixedReality.OpenXR.Samples.AnchorsSample::ToggleAirTapToCreateEnabled()
extern void AnchorsSample_ToggleAirTapToCreateEnabled_m475865F2AD75E7ACA18AB223556A432E0F57001A (void);
// 0x00000004 System.Void Microsoft.MixedReality.OpenXR.Samples.AnchorsSample::Start()
extern void AnchorsSample_Start_m6691589B0A5D1F62856A68BD488BC63B5A4F20A5 (void);
// 0x00000005 System.Void Microsoft.MixedReality.OpenXR.Samples.AnchorsSample::AnchorsChanged(UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs)
extern void AnchorsSample_AnchorsChanged_m6F8EDD358219A6B7C55A2F3701A5A5C86AB6E979 (void);
// 0x00000006 System.Void Microsoft.MixedReality.OpenXR.Samples.AnchorsSample::LateUpdate()
extern void AnchorsSample_LateUpdate_m2CB5305712067980597C11E64CF4C13155C08F52 (void);
// 0x00000007 System.Void Microsoft.MixedReality.OpenXR.Samples.AnchorsSample::OnAirTapped(UnityEngine.XR.InputDevice)
extern void AnchorsSample_OnAirTapped_m3A4025AC72D57957055BE576E4473D70B901B6F1 (void);
// 0x00000008 System.Void Microsoft.MixedReality.OpenXR.Samples.AnchorsSample::AddAnchor(UnityEngine.Pose)
extern void AnchorsSample_AddAnchor_m00FC7ABD97A4F2BF4D620C3B2FC826894BEEDD65 (void);
// 0x00000009 System.Void Microsoft.MixedReality.OpenXR.Samples.AnchorsSample::PersistOrUnpersistAnchor(UnityEngine.XR.ARFoundation.ARAnchor)
extern void AnchorsSample_PersistOrUnpersistAnchor_m6BDD1C29AEC4ACE8F5FBEAE29FD1C7A199C0F90F (void);
// 0x0000000A System.Void Microsoft.MixedReality.OpenXR.Samples.AnchorsSample::.ctor()
extern void AnchorsSample__ctor_mC49B3A03609AEB4FEC797204BA7D038627275C68 (void);
// 0x0000000B System.Void Microsoft.MixedReality.OpenXR.Samples.AnchorsSample/<Start>d__9::MoveNext()
extern void U3CStartU3Ed__9_MoveNext_m036AE33E18AFC3A69592371F52505FC14C3A420C (void);
// 0x0000000C System.Void Microsoft.MixedReality.OpenXR.Samples.AnchorsSample/<Start>d__9::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__9_SetStateMachine_m2A310C03055225064D41734583B31601B7238AC8 (void);
// 0x0000000D System.Void Microsoft.MixedReality.OpenXR.Samples.AnchorsSample/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m93C5ABC8772BDA97832D421F4255C8C05D8A9333 (void);
// 0x0000000E System.Tuple`2<System.Single,UnityEngine.XR.ARFoundation.ARAnchor> Microsoft.MixedReality.OpenXR.Samples.AnchorsSample/<>c__DisplayClass12_0::<OnAirTapped>b__0(System.Tuple`2<System.Single,UnityEngine.XR.ARFoundation.ARAnchor>,UnityEngine.XR.ARFoundation.ARAnchor)
extern void U3CU3Ec__DisplayClass12_0_U3COnAirTappedU3Eb__0_m37DA91A99B6E5E3C2262F6695165E0B5B8A7EA26 (void);
// 0x0000000F System.Void Microsoft.MixedReality.OpenXR.Samples.AppRemoting::Awake()
extern void AppRemoting_Awake_mF5CEE9BDD2F1B8BDE4FAEAF2ED75E2BDBCE7E3B8 (void);
// 0x00000010 System.Void Microsoft.MixedReality.OpenXR.Samples.AppRemoting::ConnectToRemote(System.String)
extern void AppRemoting_ConnectToRemote_m080F06203BE7B81708FDFD10C8DA78CB27086B6C (void);
// 0x00000011 System.Void Microsoft.MixedReality.OpenXR.Samples.AppRemoting::DisconnectFromRemote()
extern void AppRemoting_DisconnectFromRemote_mB106098D54AE00494C8346385864DACF77D14218 (void);
// 0x00000012 System.Void Microsoft.MixedReality.OpenXR.Samples.AppRemoting::.ctor()
extern void AppRemoting__ctor_mE2E7EFC1ABCBE1AFB8B0F6BAFA29E0704E8BE5AF (void);
// 0x00000013 System.Void Microsoft.MixedReality.OpenXR.Samples.AppRemoting::.cctor()
extern void AppRemoting__cctor_mA7686580D9AC7C751356C5817A1CD35080517AA6 (void);
// 0x00000014 System.String Microsoft.MixedReality.OpenXR.Samples.ConfigOption::get_Value()
extern void ConfigOption_get_Value_mF64F9FE6F54128B012B5BD60EC33AA68FE5CB13E (void);
// 0x00000015 System.Void Microsoft.MixedReality.OpenXR.Samples.ConfigOption::set_Value(System.String)
extern void ConfigOption_set_Value_mF7E2CA729044832C0440E28B3BAACED2042E36B5 (void);
// 0x00000016 System.String Microsoft.MixedReality.OpenXR.Samples.ConfigOption::get_Label()
// 0x00000017 System.Void Microsoft.MixedReality.OpenXR.Samples.ConfigOption::Start()
extern void ConfigOption_Start_mB6BC6B02950EE28B53C93D351C86A17447A15F9A (void);
// 0x00000018 System.Void Microsoft.MixedReality.OpenXR.Samples.ConfigOption::Initialize()
// 0x00000019 System.Void Microsoft.MixedReality.OpenXR.Samples.ConfigOption::Select()
// 0x0000001A System.Void Microsoft.MixedReality.OpenXR.Samples.ConfigOption::OnEnable()
extern void ConfigOption_OnEnable_mAFA11F4AA16B6149859628F99805B3791B4545A2 (void);
// 0x0000001B System.Void Microsoft.MixedReality.OpenXR.Samples.ConfigOption::UpdateVisuals()
extern void ConfigOption_UpdateVisuals_mB3FE68614BAEFA9D2668A94F43E9FA7EBB58E69F (void);
// 0x0000001C System.Void Microsoft.MixedReality.OpenXR.Samples.ConfigOption::.ctor()
extern void ConfigOption__ctor_m62A892B175C5B78B7CEBDB004DF16535F48BE3C4 (void);
// 0x0000001D System.String Microsoft.MixedReality.OpenXR.Samples.ConfigOptionRenderMode::get_Label()
extern void ConfigOptionRenderMode_get_Label_m359A46AA3AB72A6871A4461EE20420B14744C684 (void);
// 0x0000001E System.Void Microsoft.MixedReality.OpenXR.Samples.ConfigOptionRenderMode::Initialize()
extern void ConfigOptionRenderMode_Initialize_mB65557E4167ECC9CB0E515D93CF5BC92ECF33F86 (void);
// 0x0000001F System.Void Microsoft.MixedReality.OpenXR.Samples.ConfigOptionRenderMode::Select()
extern void ConfigOptionRenderMode_Select_mF3938D31DE31FD934A5AC02E963F0EEC1134C9E8 (void);
// 0x00000020 System.Collections.IEnumerator Microsoft.MixedReality.OpenXR.Samples.ConfigOptionRenderMode::UpdateRenderMode()
extern void ConfigOptionRenderMode_UpdateRenderMode_m345FA0DF7E031EBEDAD167D658E0208EE145364F (void);
// 0x00000021 System.Void Microsoft.MixedReality.OpenXR.Samples.ConfigOptionRenderMode::.ctor()
extern void ConfigOptionRenderMode__ctor_m753318D5B86E60E1EDDAF5EC6610F7F470678331 (void);
// 0x00000022 System.Void Microsoft.MixedReality.OpenXR.Samples.ConfigOptionRenderMode/<UpdateRenderMode>d__6::.ctor(System.Int32)
extern void U3CUpdateRenderModeU3Ed__6__ctor_mC6318A7491D164EAD72E7F8D9AD0B14456527942 (void);
// 0x00000023 System.Void Microsoft.MixedReality.OpenXR.Samples.ConfigOptionRenderMode/<UpdateRenderMode>d__6::System.IDisposable.Dispose()
extern void U3CUpdateRenderModeU3Ed__6_System_IDisposable_Dispose_m44CCF2FE6C4D273E433DAE5F82254FF8E319D317 (void);
// 0x00000024 System.Boolean Microsoft.MixedReality.OpenXR.Samples.ConfigOptionRenderMode/<UpdateRenderMode>d__6::MoveNext()
extern void U3CUpdateRenderModeU3Ed__6_MoveNext_m3E1A8314113DCD51837B548860A7288C22DD246C (void);
// 0x00000025 System.Object Microsoft.MixedReality.OpenXR.Samples.ConfigOptionRenderMode/<UpdateRenderMode>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateRenderModeU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12DFD238D3C7720320896FFE0966B1E23578D147 (void);
// 0x00000026 System.Void Microsoft.MixedReality.OpenXR.Samples.ConfigOptionRenderMode/<UpdateRenderMode>d__6::System.Collections.IEnumerator.Reset()
extern void U3CUpdateRenderModeU3Ed__6_System_Collections_IEnumerator_Reset_m33E65D502A29BFD20EE800E36E075028612624DB (void);
// 0x00000027 System.Object Microsoft.MixedReality.OpenXR.Samples.ConfigOptionRenderMode/<UpdateRenderMode>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateRenderModeU3Ed__6_System_Collections_IEnumerator_get_Current_m9EA6DFD6CCB8260B2A7DFB06A06F6C7238F126A8 (void);
// 0x00000028 System.Void Microsoft.MixedReality.OpenXR.Samples.FollowEyeGaze::ToggleFollow()
extern void FollowEyeGaze_ToggleFollow_mCAAB0CD7419B13B6B607043BD29269D1CA6150D4 (void);
// 0x00000029 System.Void Microsoft.MixedReality.OpenXR.Samples.FollowEyeGaze::Awake()
extern void FollowEyeGaze_Awake_m7E208265BD7F47442CC4BD5AE6C5DF3023C86BD7 (void);
// 0x0000002A System.Void Microsoft.MixedReality.OpenXR.Samples.FollowEyeGaze::Update()
extern void FollowEyeGaze_Update_m3B3876362838CB3CB5504B159E51CE692696C352 (void);
// 0x0000002B System.Void Microsoft.MixedReality.OpenXR.Samples.FollowEyeGaze::.ctor()
extern void FollowEyeGaze__ctor_m6F5A73C94DFF6555460D3CD2D2F44FBBA8D2D67B (void);
// 0x0000002C System.Void Microsoft.MixedReality.OpenXR.Samples.FollowEyeGaze::.cctor()
extern void FollowEyeGaze__cctor_mC4972BA708BA2BE793B86E33066E27F66E1FAD1E (void);
// 0x0000002D System.Void Microsoft.MixedReality.OpenXR.Samples.Hand::.ctor(UnityEngine.GameObject)
extern void Hand__ctor_m24AA14C946FB198B934411CA0ED009557DCF61D9 (void);
// 0x0000002E System.Boolean Microsoft.MixedReality.OpenXR.Samples.Hand::InstantiateJointPrefab(UnityEngine.GameObject&)
extern void Hand_InstantiateJointPrefab_m989965E68852563898C8B9ADDDA7DCA618D39D66 (void);
// 0x0000002F System.Void Microsoft.MixedReality.OpenXR.Samples.Hand::DisableHandJoints()
extern void Hand_DisableHandJoints_m9347C67997E0A26237AA6B77ADC474FFF5D9719B (void);
// 0x00000030 System.Void Microsoft.MixedReality.OpenXR.Samples.Hand::UpdateHandJoints(Microsoft.MixedReality.OpenXR.HandJointLocation[])
extern void Hand_UpdateHandJoints_m73E4C0D96C4C205892D73500D50D3AD043FA4546 (void);
// 0x00000031 System.Void Microsoft.MixedReality.OpenXR.Samples.Hand::UpdateHandJoints(UnityEngine.XR.Hand)
extern void Hand_UpdateHandJoints_mA50E731C055EB56CBD5C98A563ECF83EC9B189F1 (void);
// 0x00000032 System.Void Microsoft.MixedReality.OpenXR.Samples.Hand::ColorJointObject(UnityEngine.GameObject,System.Nullable`1<UnityEngine.XR.HandFinger>,System.Nullable`1<System.Int32>)
extern void Hand_ColorJointObject_mC5436AA5159B9B5BD038F3FEB9539F341FD1F68E (void);
// 0x00000033 System.Nullable`1<UnityEngine.XR.HandFinger> Microsoft.MixedReality.OpenXR.Samples.Hand::GetHandFinger(Microsoft.MixedReality.OpenXR.HandJoint)
extern void Hand_GetHandFinger_m2563C439F2D5077EE1FDE9FF167E3FC94DD95C84 (void);
// 0x00000034 System.Nullable`1<System.Int32> Microsoft.MixedReality.OpenXR.Samples.Hand::GetIndexOnFinger(Microsoft.MixedReality.OpenXR.HandJoint)
extern void Hand_GetIndexOnFinger_m8570CC5D684BE9B127493B2B35A1D0D597A8106D (void);
// 0x00000035 System.Void Microsoft.MixedReality.OpenXR.Samples.Hand::.cctor()
extern void Hand__cctor_m80F68CBB43A1F058B001E97BAB0C896ABE2D68C0 (void);
// 0x00000036 System.Void Microsoft.MixedReality.OpenXR.Samples.HandJointsManager::OnEnable()
extern void HandJointsManager_OnEnable_mE66563F05ED56616902CB11EE600482FE44A1ACB (void);
// 0x00000037 System.Void Microsoft.MixedReality.OpenXR.Samples.HandJointsManager::OnDisable()
extern void HandJointsManager_OnDisable_mDF63AEC81AFBF22CDBAB51F1E4DC3F6484F89923 (void);
// 0x00000038 System.Void Microsoft.MixedReality.OpenXR.Samples.HandJointsManager::Application_onBeforeRender()
extern void HandJointsManager_Application_onBeforeRender_m1F7125ACFBA9BF305CAB3D191BCEAD752E1C43E2 (void);
// 0x00000039 System.Void Microsoft.MixedReality.OpenXR.Samples.HandJointsManager::Start()
extern void HandJointsManager_Start_mCBE48D05102D1DEBBBFFE43DB09F9AB444EA94B8 (void);
// 0x0000003A System.Void Microsoft.MixedReality.OpenXR.Samples.HandJointsManager::Update()
extern void HandJointsManager_Update_m3A30D62C2FD1518FA7A18B6A2838645877724275 (void);
// 0x0000003B System.Void Microsoft.MixedReality.OpenXR.Samples.HandJointsManager::UpdateHandJointsUsingFeatureUsage(UnityEngine.XR.InputDeviceCharacteristics,Microsoft.MixedReality.OpenXR.Samples.Hand)
extern void HandJointsManager_UpdateHandJointsUsingFeatureUsage_m46D3294BBC67CAE87A6E1D420FF7E14ECB00854B (void);
// 0x0000003C System.Void Microsoft.MixedReality.OpenXR.Samples.HandJointsManager::UpdateHandJointsUsingOpenXRExtension(Microsoft.MixedReality.OpenXR.HandTracker,Microsoft.MixedReality.OpenXR.Samples.Hand,Microsoft.MixedReality.OpenXR.FrameTime)
extern void HandJointsManager_UpdateHandJointsUsingOpenXRExtension_mFE364B69850B2986B2A016EB2021F84A3A6EE50A (void);
// 0x0000003D System.Void Microsoft.MixedReality.OpenXR.Samples.HandJointsManager::.ctor()
extern void HandJointsManager__ctor_mA8A918D38E0F579B53894A3667B09F8E6D11EFDD (void);
// 0x0000003E System.Void Microsoft.MixedReality.OpenXR.Samples.HandJointsManager::.cctor()
extern void HandJointsManager__cctor_mDF26F10E02B115710DCB22AB833586190DD998C6 (void);
// 0x0000003F System.Void Microsoft.MixedReality.OpenXR.Samples.HandMesh::Awake()
extern void HandMesh_Awake_m81EA68A84EDF45469AC20C25F9E6B46E50E4B3F7 (void);
// 0x00000040 System.Void Microsoft.MixedReality.OpenXR.Samples.HandMesh::Update()
extern void HandMesh_Update_mA45F2FF403C63F4BC5C81A96B0428682661392CC (void);
// 0x00000041 System.Void Microsoft.MixedReality.OpenXR.Samples.HandMesh::.ctor()
extern void HandMesh__ctor_m7E4C0E79717708CAD52281ADAAD7A3F16C6C825E (void);
// 0x00000042 System.Void Microsoft.MixedReality.OpenXR.Samples.LocatableCamera::Start()
extern void LocatableCamera_Start_mAACA4FAA34193DFB8E2AF0A1B5F80BCF599D6580 (void);
// 0x00000043 System.Void Microsoft.MixedReality.OpenXR.Samples.LocatableCamera::OnDestroy()
extern void LocatableCamera_OnDestroy_mBCEC5C69433E98D54DC46AECC613FBC209444C6D (void);
// 0x00000044 System.Void Microsoft.MixedReality.OpenXR.Samples.LocatableCamera::OnPhotoCaptureCreated(UnityEngine.Windows.WebCam.PhotoCapture)
extern void LocatableCamera_OnPhotoCaptureCreated_m67BB7F8AFCC080A003AC861635B3AF7F8BFEA0A8 (void);
// 0x00000045 System.Void Microsoft.MixedReality.OpenXR.Samples.LocatableCamera::OnPhotoModeStarted(UnityEngine.Windows.WebCam.PhotoCapture/PhotoCaptureResult)
extern void LocatableCamera_OnPhotoModeStarted_m659CFFE4FD8012A9C505959F2C489E4368C8F810 (void);
// 0x00000046 System.Void Microsoft.MixedReality.OpenXR.Samples.LocatableCamera::TakePhoto()
extern void LocatableCamera_TakePhoto_mB6434261F327B384CBD31B543B785CD98EF131B0 (void);
// 0x00000047 System.Void Microsoft.MixedReality.OpenXR.Samples.LocatableCamera::OnPhotoCaptured(UnityEngine.Windows.WebCam.PhotoCapture/PhotoCaptureResult,UnityEngine.Windows.WebCam.PhotoCaptureFrame)
extern void LocatableCamera_OnPhotoCaptured_m31C48AFDD132D62E57275785978BC1F1B5FD68F7 (void);
// 0x00000048 System.Void Microsoft.MixedReality.OpenXR.Samples.LocatableCamera::OnPhotoCaptureStopped(UnityEngine.Windows.WebCam.PhotoCapture/PhotoCaptureResult)
extern void LocatableCamera_OnPhotoCaptureStopped_m72470023F14459D8F7D5C3AA6A408692F2B263CA (void);
// 0x00000049 System.Void Microsoft.MixedReality.OpenXR.Samples.LocatableCamera::.ctor()
extern void LocatableCamera__ctor_m4654D235E3DD92F2652A85370F21F4901D643B33 (void);
// 0x0000004A System.Void Microsoft.MixedReality.OpenXR.Samples.LocatableCamera/<>c::.cctor()
extern void U3CU3Ec__cctor_m00AA4EEA238BE45BC52A0C293C26DF94F1804C13 (void);
// 0x0000004B System.Void Microsoft.MixedReality.OpenXR.Samples.LocatableCamera/<>c::.ctor()
extern void U3CU3Ec__ctor_m1394A047F14EBA712AA06C1FE0F53FB72FE51FF8 (void);
// 0x0000004C System.Int32 Microsoft.MixedReality.OpenXR.Samples.LocatableCamera/<>c::<Start>b__7_0(UnityEngine.Resolution)
extern void U3CU3Ec_U3CStartU3Eb__7_0_m47D9C867D9012F3DBE2A7C690E86F2C57BE010C1 (void);
// 0x0000004D System.Void Microsoft.MixedReality.OpenXR.Samples.LoggingPanel::LogText(System.String,System.Boolean)
extern void LoggingPanel_LogText_m2BDA2D8A3569701DCE18D4057F847F7D9EBE41A6 (void);
// 0x0000004E System.Void Microsoft.MixedReality.OpenXR.Samples.LoggingPanel::OnEnable()
extern void LoggingPanel_OnEnable_m275EAD631BC4552F263C22AD42E23E4D391D51D3 (void);
// 0x0000004F System.Void Microsoft.MixedReality.OpenXR.Samples.LoggingPanel::OnDisable()
extern void LoggingPanel_OnDisable_m80980DC29D8433CD69B84585244CABA8D48CF165 (void);
// 0x00000050 System.Void Microsoft.MixedReality.OpenXR.Samples.LoggingPanel::LogUnityMessage(System.String,System.String,UnityEngine.LogType)
extern void LoggingPanel_LogUnityMessage_mA0794B991966D0A030E1C613A8447C3F272E0B4D (void);
// 0x00000051 System.Void Microsoft.MixedReality.OpenXR.Samples.LoggingPanel::.ctor()
extern void LoggingPanel__ctor_m5A1E367802F6EA287380536ADBC89E129BF927B7 (void);
// 0x00000052 System.Void Microsoft.MixedReality.OpenXR.Samples.LoggingPanel/<>c::.cctor()
extern void U3CU3Ec__cctor_m7D6A1100174D582EC2182C37AAC94C368C8D3777 (void);
// 0x00000053 System.Void Microsoft.MixedReality.OpenXR.Samples.LoggingPanel/<>c::.ctor()
extern void U3CU3Ec__ctor_m7F6CE70FC22FFDE914E06D1F1BA6526D650AEA49 (void);
// 0x00000054 System.Text.StringBuilder Microsoft.MixedReality.OpenXR.Samples.LoggingPanel/<>c::<LogText>b__3_0(System.Text.StringBuilder,System.String)
extern void U3CU3Ec_U3CLogTextU3Eb__3_0_m2D6F29E40571D7CCEE2232E07132F2CD4A0F5592 (void);
// 0x00000055 System.String Microsoft.MixedReality.OpenXR.Samples.LoggingPanel/<>c::<LogText>b__3_1(System.Text.StringBuilder)
extern void U3CU3Ec_U3CLogTextU3Eb__3_1_m6086F144DAB8804A2456981CC1F2E78BA64A0165 (void);
// 0x00000056 System.Void Microsoft.MixedReality.OpenXR.Samples.OpenXRSpace::Start()
extern void OpenXRSpace_Start_mF1D1F458BBF6CFF5DED2AA7FF4A3C59F944DB43F (void);
// 0x00000057 System.Void Microsoft.MixedReality.OpenXR.Samples.OpenXRSpace::Application_onBeforeRender()
extern void OpenXRSpace_Application_onBeforeRender_mEB19B161D53B02E863DC9558C32A36974DE172DB (void);
// 0x00000058 System.Void Microsoft.MixedReality.OpenXR.Samples.OpenXRSpace::SetTrackerSpace()
extern void OpenXRSpace_SetTrackerSpace_m1EEBAB2903B8C0A1CE925B87D7664C6453E59177 (void);
// 0x00000059 System.Void Microsoft.MixedReality.OpenXR.Samples.OpenXRSpace::EnsureTrackedPoseDriver()
extern void OpenXRSpace_EnsureTrackedPoseDriver_mE895DAB9619D8B62BC38F4B3C6FDB245657376AA (void);
// 0x0000005A System.Void Microsoft.MixedReality.OpenXR.Samples.OpenXRSpace::RemoveTrackedPosedDriver()
extern void OpenXRSpace_RemoveTrackedPosedDriver_m9EFCC6B19B1DCED84D0BBF64DDAF43648BB699A8 (void);
// 0x0000005B System.Void Microsoft.MixedReality.OpenXR.Samples.OpenXRSpace::.ctor()
extern void OpenXRSpace__ctor_mEE21D5B8EA289CDF59322C0AA09482FB5DA29BE1 (void);
// 0x0000005C UnityEngine.Material Microsoft.MixedReality.OpenXR.Samples.PlaneMaterialLookup::GetMaterialFromClassification(UnityEngine.XR.ARSubsystems.PlaneClassification)
extern void PlaneMaterialLookup_GetMaterialFromClassification_m770CD803FA4B23F07858ECC6F34A280CA8535EFB (void);
// 0x0000005D System.Void Microsoft.MixedReality.OpenXR.Samples.PlaneMaterialLookup::.ctor()
extern void PlaneMaterialLookup__ctor_mAD0F63D720A07BF28917ECF79728A2A2AAD66E86 (void);
// 0x0000005E System.Void Microsoft.MixedReality.OpenXR.Samples.PlaneMaterialLookup/PlaneClassificationPair::.ctor()
extern void PlaneClassificationPair__ctor_mE5E15BFD378B37EEBA98E656241D702FF358B716 (void);
// 0x0000005F System.String Microsoft.MixedReality.OpenXR.Samples.SampleAnchor::get_Name()
extern void SampleAnchor_get_Name_mBB2E3020FBB5A6FD410FEE3E24BB1E9530728815 (void);
// 0x00000060 System.Void Microsoft.MixedReality.OpenXR.Samples.SampleAnchor::set_Name(System.String)
extern void SampleAnchor_set_Name_mA179C24E54D39E6569EC5495BF296A811335CA89 (void);
// 0x00000061 System.Boolean Microsoft.MixedReality.OpenXR.Samples.SampleAnchor::get_Persisted()
extern void SampleAnchor_get_Persisted_mBFBD85A447848BAC1DEC1F7AC10DCBDA91C35AD4 (void);
// 0x00000062 System.Void Microsoft.MixedReality.OpenXR.Samples.SampleAnchor::set_Persisted(System.Boolean)
extern void SampleAnchor_set_Persisted_mFCC546DC360C3000CB24F8CDDAEE0C2FA51B123E (void);
// 0x00000063 System.Void Microsoft.MixedReality.OpenXR.Samples.SampleAnchor::UpdateText()
extern void SampleAnchor_UpdateText_m0744E736443F0D3C5C53661E8A8E8F3F80E2018E (void);
// 0x00000064 System.Void Microsoft.MixedReality.OpenXR.Samples.SampleAnchor::Start()
extern void SampleAnchor_Start_m080F2AE16E3C81082BC132272CCA06CBC8702B40 (void);
// 0x00000065 System.Void Microsoft.MixedReality.OpenXR.Samples.SampleAnchor::.ctor()
extern void SampleAnchor__ctor_m34F15D021E8621BFD0947AE7F6D5475FF526827D (void);
// 0x00000066 System.Void Microsoft.MixedReality.OpenXR.Samples.SceneLoader::LoadScene(System.String)
extern void SceneLoader_LoadScene_m0316600F39F626481FCEC8C8579B0778C7AF1BDC (void);
// 0x00000067 System.Void Microsoft.MixedReality.OpenXR.Samples.SceneLoader::.ctor()
extern void SceneLoader__ctor_m48297E9BD3B44C4F00E44ADC55400DD6800A64B6 (void);
// 0x00000068 System.Void Microsoft.MixedReality.OpenXR.Samples.Tappable::Update()
extern void Tappable_Update_m913CBBBD4383BD6D283DEC5FE15BCD97F1DDA4F1 (void);
// 0x00000069 System.Void Microsoft.MixedReality.OpenXR.Samples.Tappable::.ctor()
extern void Tappable__ctor_m4C828B3CA3BA71CB2D4C6AF98B64475A4237744D (void);
static Il2CppMethodPointer s_methodPointers[105] = 
{
	RuntimeInfo_Start_m1D3D82223508B59B3D9CABC900EEFBF84AF839E6,
	RuntimeInfo__ctor_m2E2554F3B318E8B3BF85A4B412CF9B11B9AF4BE4,
	AnchorsSample_ToggleAirTapToCreateEnabled_m475865F2AD75E7ACA18AB223556A432E0F57001A,
	AnchorsSample_Start_m6691589B0A5D1F62856A68BD488BC63B5A4F20A5,
	AnchorsSample_AnchorsChanged_m6F8EDD358219A6B7C55A2F3701A5A5C86AB6E979,
	AnchorsSample_LateUpdate_m2CB5305712067980597C11E64CF4C13155C08F52,
	AnchorsSample_OnAirTapped_m3A4025AC72D57957055BE576E4473D70B901B6F1,
	AnchorsSample_AddAnchor_m00FC7ABD97A4F2BF4D620C3B2FC826894BEEDD65,
	AnchorsSample_PersistOrUnpersistAnchor_m6BDD1C29AEC4ACE8F5FBEAE29FD1C7A199C0F90F,
	AnchorsSample__ctor_mC49B3A03609AEB4FEC797204BA7D038627275C68,
	U3CStartU3Ed__9_MoveNext_m036AE33E18AFC3A69592371F52505FC14C3A420C,
	U3CStartU3Ed__9_SetStateMachine_m2A310C03055225064D41734583B31601B7238AC8,
	U3CU3Ec__DisplayClass12_0__ctor_m93C5ABC8772BDA97832D421F4255C8C05D8A9333,
	U3CU3Ec__DisplayClass12_0_U3COnAirTappedU3Eb__0_m37DA91A99B6E5E3C2262F6695165E0B5B8A7EA26,
	AppRemoting_Awake_mF5CEE9BDD2F1B8BDE4FAEAF2ED75E2BDBCE7E3B8,
	AppRemoting_ConnectToRemote_m080F06203BE7B81708FDFD10C8DA78CB27086B6C,
	AppRemoting_DisconnectFromRemote_mB106098D54AE00494C8346385864DACF77D14218,
	AppRemoting__ctor_mE2E7EFC1ABCBE1AFB8B0F6BAFA29E0704E8BE5AF,
	AppRemoting__cctor_mA7686580D9AC7C751356C5817A1CD35080517AA6,
	ConfigOption_get_Value_mF64F9FE6F54128B012B5BD60EC33AA68FE5CB13E,
	ConfigOption_set_Value_mF7E2CA729044832C0440E28B3BAACED2042E36B5,
	NULL,
	ConfigOption_Start_mB6BC6B02950EE28B53C93D351C86A17447A15F9A,
	NULL,
	NULL,
	ConfigOption_OnEnable_mAFA11F4AA16B6149859628F99805B3791B4545A2,
	ConfigOption_UpdateVisuals_mB3FE68614BAEFA9D2668A94F43E9FA7EBB58E69F,
	ConfigOption__ctor_m62A892B175C5B78B7CEBDB004DF16535F48BE3C4,
	ConfigOptionRenderMode_get_Label_m359A46AA3AB72A6871A4461EE20420B14744C684,
	ConfigOptionRenderMode_Initialize_mB65557E4167ECC9CB0E515D93CF5BC92ECF33F86,
	ConfigOptionRenderMode_Select_mF3938D31DE31FD934A5AC02E963F0EEC1134C9E8,
	ConfigOptionRenderMode_UpdateRenderMode_m345FA0DF7E031EBEDAD167D658E0208EE145364F,
	ConfigOptionRenderMode__ctor_m753318D5B86E60E1EDDAF5EC6610F7F470678331,
	U3CUpdateRenderModeU3Ed__6__ctor_mC6318A7491D164EAD72E7F8D9AD0B14456527942,
	U3CUpdateRenderModeU3Ed__6_System_IDisposable_Dispose_m44CCF2FE6C4D273E433DAE5F82254FF8E319D317,
	U3CUpdateRenderModeU3Ed__6_MoveNext_m3E1A8314113DCD51837B548860A7288C22DD246C,
	U3CUpdateRenderModeU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12DFD238D3C7720320896FFE0966B1E23578D147,
	U3CUpdateRenderModeU3Ed__6_System_Collections_IEnumerator_Reset_m33E65D502A29BFD20EE800E36E075028612624DB,
	U3CUpdateRenderModeU3Ed__6_System_Collections_IEnumerator_get_Current_m9EA6DFD6CCB8260B2A7DFB06A06F6C7238F126A8,
	FollowEyeGaze_ToggleFollow_mCAAB0CD7419B13B6B607043BD29269D1CA6150D4,
	FollowEyeGaze_Awake_m7E208265BD7F47442CC4BD5AE6C5DF3023C86BD7,
	FollowEyeGaze_Update_m3B3876362838CB3CB5504B159E51CE692696C352,
	FollowEyeGaze__ctor_m6F5A73C94DFF6555460D3CD2D2F44FBBA8D2D67B,
	FollowEyeGaze__cctor_mC4972BA708BA2BE793B86E33066E27F66E1FAD1E,
	Hand__ctor_m24AA14C946FB198B934411CA0ED009557DCF61D9,
	Hand_InstantiateJointPrefab_m989965E68852563898C8B9ADDDA7DCA618D39D66,
	Hand_DisableHandJoints_m9347C67997E0A26237AA6B77ADC474FFF5D9719B,
	Hand_UpdateHandJoints_m73E4C0D96C4C205892D73500D50D3AD043FA4546,
	Hand_UpdateHandJoints_mA50E731C055EB56CBD5C98A563ECF83EC9B189F1,
	Hand_ColorJointObject_mC5436AA5159B9B5BD038F3FEB9539F341FD1F68E,
	Hand_GetHandFinger_m2563C439F2D5077EE1FDE9FF167E3FC94DD95C84,
	Hand_GetIndexOnFinger_m8570CC5D684BE9B127493B2B35A1D0D597A8106D,
	Hand__cctor_m80F68CBB43A1F058B001E97BAB0C896ABE2D68C0,
	HandJointsManager_OnEnable_mE66563F05ED56616902CB11EE600482FE44A1ACB,
	HandJointsManager_OnDisable_mDF63AEC81AFBF22CDBAB51F1E4DC3F6484F89923,
	HandJointsManager_Application_onBeforeRender_m1F7125ACFBA9BF305CAB3D191BCEAD752E1C43E2,
	HandJointsManager_Start_mCBE48D05102D1DEBBBFFE43DB09F9AB444EA94B8,
	HandJointsManager_Update_m3A30D62C2FD1518FA7A18B6A2838645877724275,
	HandJointsManager_UpdateHandJointsUsingFeatureUsage_m46D3294BBC67CAE87A6E1D420FF7E14ECB00854B,
	HandJointsManager_UpdateHandJointsUsingOpenXRExtension_mFE364B69850B2986B2A016EB2021F84A3A6EE50A,
	HandJointsManager__ctor_mA8A918D38E0F579B53894A3667B09F8E6D11EFDD,
	HandJointsManager__cctor_mDF26F10E02B115710DCB22AB833586190DD998C6,
	HandMesh_Awake_m81EA68A84EDF45469AC20C25F9E6B46E50E4B3F7,
	HandMesh_Update_mA45F2FF403C63F4BC5C81A96B0428682661392CC,
	HandMesh__ctor_m7E4C0E79717708CAD52281ADAAD7A3F16C6C825E,
	LocatableCamera_Start_mAACA4FAA34193DFB8E2AF0A1B5F80BCF599D6580,
	LocatableCamera_OnDestroy_mBCEC5C69433E98D54DC46AECC613FBC209444C6D,
	LocatableCamera_OnPhotoCaptureCreated_m67BB7F8AFCC080A003AC861635B3AF7F8BFEA0A8,
	LocatableCamera_OnPhotoModeStarted_m659CFFE4FD8012A9C505959F2C489E4368C8F810,
	LocatableCamera_TakePhoto_mB6434261F327B384CBD31B543B785CD98EF131B0,
	LocatableCamera_OnPhotoCaptured_m31C48AFDD132D62E57275785978BC1F1B5FD68F7,
	LocatableCamera_OnPhotoCaptureStopped_m72470023F14459D8F7D5C3AA6A408692F2B263CA,
	LocatableCamera__ctor_m4654D235E3DD92F2652A85370F21F4901D643B33,
	U3CU3Ec__cctor_m00AA4EEA238BE45BC52A0C293C26DF94F1804C13,
	U3CU3Ec__ctor_m1394A047F14EBA712AA06C1FE0F53FB72FE51FF8,
	U3CU3Ec_U3CStartU3Eb__7_0_m47D9C867D9012F3DBE2A7C690E86F2C57BE010C1,
	LoggingPanel_LogText_m2BDA2D8A3569701DCE18D4057F847F7D9EBE41A6,
	LoggingPanel_OnEnable_m275EAD631BC4552F263C22AD42E23E4D391D51D3,
	LoggingPanel_OnDisable_m80980DC29D8433CD69B84585244CABA8D48CF165,
	LoggingPanel_LogUnityMessage_mA0794B991966D0A030E1C613A8447C3F272E0B4D,
	LoggingPanel__ctor_m5A1E367802F6EA287380536ADBC89E129BF927B7,
	U3CU3Ec__cctor_m7D6A1100174D582EC2182C37AAC94C368C8D3777,
	U3CU3Ec__ctor_m7F6CE70FC22FFDE914E06D1F1BA6526D650AEA49,
	U3CU3Ec_U3CLogTextU3Eb__3_0_m2D6F29E40571D7CCEE2232E07132F2CD4A0F5592,
	U3CU3Ec_U3CLogTextU3Eb__3_1_m6086F144DAB8804A2456981CC1F2E78BA64A0165,
	OpenXRSpace_Start_mF1D1F458BBF6CFF5DED2AA7FF4A3C59F944DB43F,
	OpenXRSpace_Application_onBeforeRender_mEB19B161D53B02E863DC9558C32A36974DE172DB,
	OpenXRSpace_SetTrackerSpace_m1EEBAB2903B8C0A1CE925B87D7664C6453E59177,
	OpenXRSpace_EnsureTrackedPoseDriver_mE895DAB9619D8B62BC38F4B3C6FDB245657376AA,
	OpenXRSpace_RemoveTrackedPosedDriver_m9EFCC6B19B1DCED84D0BBF64DDAF43648BB699A8,
	OpenXRSpace__ctor_mEE21D5B8EA289CDF59322C0AA09482FB5DA29BE1,
	PlaneMaterialLookup_GetMaterialFromClassification_m770CD803FA4B23F07858ECC6F34A280CA8535EFB,
	PlaneMaterialLookup__ctor_mAD0F63D720A07BF28917ECF79728A2A2AAD66E86,
	PlaneClassificationPair__ctor_mE5E15BFD378B37EEBA98E656241D702FF358B716,
	SampleAnchor_get_Name_mBB2E3020FBB5A6FD410FEE3E24BB1E9530728815,
	SampleAnchor_set_Name_mA179C24E54D39E6569EC5495BF296A811335CA89,
	SampleAnchor_get_Persisted_mBFBD85A447848BAC1DEC1F7AC10DCBDA91C35AD4,
	SampleAnchor_set_Persisted_mFCC546DC360C3000CB24F8CDDAEE0C2FA51B123E,
	SampleAnchor_UpdateText_m0744E736443F0D3C5C53661E8A8E8F3F80E2018E,
	SampleAnchor_Start_m080F2AE16E3C81082BC132272CCA06CBC8702B40,
	SampleAnchor__ctor_m34F15D021E8621BFD0947AE7F6D5475FF526827D,
	SceneLoader_LoadScene_m0316600F39F626481FCEC8C8579B0778C7AF1BDC,
	SceneLoader__ctor_m48297E9BD3B44C4F00E44ADC55400DD6800A64B6,
	Tappable_Update_m913CBBBD4383BD6D283DEC5FE15BCD97F1DDA4F1,
	Tappable__ctor_m4C828B3CA3BA71CB2D4C6AF98B64475A4237744D,
};
extern void U3CStartU3Ed__9_MoveNext_m036AE33E18AFC3A69592371F52505FC14C3A420C_AdjustorThunk (void);
extern void U3CStartU3Ed__9_SetStateMachine_m2A310C03055225064D41734583B31601B7238AC8_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x0600000B, U3CStartU3Ed__9_MoveNext_m036AE33E18AFC3A69592371F52505FC14C3A420C_AdjustorThunk },
	{ 0x0600000C, U3CStartU3Ed__9_SetStateMachine_m2A310C03055225064D41734583B31601B7238AC8_AdjustorThunk },
};
static const int32_t s_InvokerIndices[105] = 
{
	4910,
	4910,
	4910,
	4910,
	3848,
	4910,
	3939,
	3995,
	3982,
	4910,
	4910,
	3982,
	4910,
	1552,
	4910,
	3982,
	4910,
	4910,
	7190,
	4825,
	3982,
	4825,
	4910,
	4910,
	4910,
	4910,
	4910,
	4910,
	4825,
	4910,
	4910,
	4825,
	4910,
	3948,
	4910,
	4863,
	4825,
	4910,
	4825,
	4910,
	4910,
	4910,
	4910,
	7190,
	3982,
	3281,
	4910,
	3982,
	3920,
	6090,
	6699,
	6707,
	7190,
	4910,
	4910,
	4910,
	4910,
	4910,
	6564,
	6111,
	4910,
	7190,
	4910,
	4910,
	4910,
	4910,
	4910,
	3982,
	4138,
	4910,
	2331,
	4138,
	4910,
	7190,
	4910,
	2813,
	2261,
	4910,
	4910,
	1226,
	4910,
	7190,
	4910,
	1552,
	3029,
	4910,
	4910,
	4910,
	4910,
	4910,
	4910,
	3023,
	4910,
	4910,
	4825,
	3982,
	4863,
	4015,
	4910,
	4910,
	4910,
	3982,
	4910,
	4910,
	4910,
};
extern const CustomAttributesCacheGenerator g_Microsoft_MixedReality_OpenXR_MainSample_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Microsoft_MixedReality_OpenXR_MainSample_CodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_OpenXR_MainSample_CodeGenModule = 
{
	"Microsoft.MixedReality.OpenXR.MainSample.dll",
	105,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Microsoft_MixedReality_OpenXR_MainSample_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
