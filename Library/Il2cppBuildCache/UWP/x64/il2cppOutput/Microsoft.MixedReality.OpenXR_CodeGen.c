﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Threading.Tasks.Task`1<Microsoft.MixedReality.ARSubsystems.XRAnchorStore> Microsoft.MixedReality.ARSubsystems.AnchorSubsystemExtensions::LoadAnchorStoreAsync(UnityEngine.XR.ARSubsystems.XRAnchorSubsystem)
extern void AnchorSubsystemExtensions_LoadAnchorStoreAsync_m8873DC6588F53E4AE69655C59A89D21FA5C99240 (void);
// 0x00000002 System.Threading.Tasks.Task`1<Microsoft.MixedReality.ARSubsystems.XRAnchorStore> Microsoft.MixedReality.ARSubsystems.XRAnchorStore::LoadAsync(UnityEngine.XR.ARSubsystems.XRAnchorSubsystem)
extern void XRAnchorStore_LoadAsync_mEAD20EBFFF7B1772B5425B291F795CF2EDCD4BD2 (void);
// 0x00000003 System.Void Microsoft.MixedReality.ARSubsystems.XRAnchorStore::.ctor(Microsoft.MixedReality.OpenXR.OpenXRAnchorStore)
extern void XRAnchorStore__ctor_m2F2798339F8F3C5C3BCCBEF2DCA5D90BAEA414FB (void);
// 0x00000004 System.Boolean Microsoft.MixedReality.ARSubsystems.XRAnchorStore::TryPersistAnchor(System.String,UnityEngine.XR.ARSubsystems.TrackableId)
extern void XRAnchorStore_TryPersistAnchor_m8E9A497BD02EA856586E1D6775164BA9AC63A7B0 (void);
// 0x00000005 System.Void Microsoft.MixedReality.ARSubsystems.XRAnchorStore/<LoadAsync>d__0::MoveNext()
extern void U3CLoadAsyncU3Ed__0_MoveNext_mD07CA3DEB02D0D731328F87FAEEBDE336B391476 (void);
// 0x00000006 System.Void Microsoft.MixedReality.ARSubsystems.XRAnchorStore/<LoadAsync>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadAsyncU3Ed__0_SetStateMachine_m02FDE1D534549B190ABD2C8C23D1AE181F847620 (void);
// 0x00000007 System.UInt64 Microsoft.MixedReality.OpenXR.AnchorConverter::ToOpenXRHandle(System.IntPtr)
extern void AnchorConverter_ToOpenXRHandle_m0DF0F055393DDAA5A4CD5AB1887EB66CF609B589 (void);
// 0x00000008 System.Object Microsoft.MixedReality.OpenXR.AnchorConverter::ToPerceptionSpatialAnchor(System.IntPtr)
extern void AnchorConverter_ToPerceptionSpatialAnchor_m3D2AFD16D67E9A618EA6AFBFBC0AF081CD2FC9F5 (void);
// 0x00000009 System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::OnEnable()
extern void EyeLevelSceneOrigin_OnEnable_m6BCDA1FB726B403592863A994349A2981D7BF1F6 (void);
// 0x0000000A System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::OnDisable()
extern void EyeLevelSceneOrigin_OnDisable_m86AA78502E55ED72AD3C26A2C54E831EED0C1059 (void);
// 0x0000000B System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::XrInput_trackingOriginUpdated(UnityEngine.XR.XRInputSubsystem)
extern void EyeLevelSceneOrigin_XrInput_trackingOriginUpdated_m406EDC09F5C2F1F2D28FCE2D3B0D2D65D4BEABC3 (void);
// 0x0000000C System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::EnsureSceneOriginAtEyeLevel()
extern void EyeLevelSceneOrigin_EnsureSceneOriginAtEyeLevel_mE8D73598B12357982C49A7003670C499C95025C0 (void);
// 0x0000000D System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::SetEyeLevelTrackingOriginMode(UnityEngine.XR.XRInputSubsystem)
extern void EyeLevelSceneOrigin_SetEyeLevelTrackingOriginMode_m271BC6E7A37A791FA92FDB012377488A08B412D8 (void);
// 0x0000000E System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::.ctor()
extern void EyeLevelSceneOrigin__ctor_mBF34B058AC130FFFAC4905DF4606902326EEDBA4 (void);
// 0x0000000F Microsoft.MixedReality.OpenXR.HandMeshTracker Microsoft.MixedReality.OpenXR.HandMeshTracker::get_Left()
extern void HandMeshTracker_get_Left_m03C9D512E0686B3332AAA1BBB00222B1EEBC4CE8 (void);
// 0x00000010 Microsoft.MixedReality.OpenXR.HandMeshTracker Microsoft.MixedReality.OpenXR.HandMeshTracker::get_Right()
extern void HandMeshTracker_get_Right_m75A2B30E198B61E3B45977819080BD7ED16BCEDE (void);
// 0x00000011 System.Void Microsoft.MixedReality.OpenXR.HandMeshTracker::.ctor(Microsoft.MixedReality.OpenXR.Handedness)
extern void HandMeshTracker__ctor_mF9F1B43E432F8623BE61321740BF30FD04CB700C (void);
// 0x00000012 System.Boolean Microsoft.MixedReality.OpenXR.HandMeshTracker::TryLocateHandMesh(Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Pose&,Microsoft.MixedReality.OpenXR.HandPoseType)
extern void HandMeshTracker_TryLocateHandMesh_m1CF196C1E53899409F7A6C0CD87527A83AE10082 (void);
// 0x00000013 System.Boolean Microsoft.MixedReality.OpenXR.HandMeshTracker::TryGetHandMesh(Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Mesh,Microsoft.MixedReality.OpenXR.HandPoseType)
extern void HandMeshTracker_TryGetHandMesh_m8BACC3586977F84E3B20B7E9FB7CC105EC477FB4 (void);
// 0x00000014 System.Void Microsoft.MixedReality.OpenXR.HandMeshTracker::.cctor()
extern void HandMeshTracker__cctor_mEE2DB60BBF234CEA5D9483474787B00D1EB9D843 (void);
// 0x00000015 Microsoft.MixedReality.OpenXR.HandTracker Microsoft.MixedReality.OpenXR.HandTracker::get_Left()
extern void HandTracker_get_Left_m78E03ADBE7621FBB3508DDC93687FB739E938344 (void);
// 0x00000016 Microsoft.MixedReality.OpenXR.HandTracker Microsoft.MixedReality.OpenXR.HandTracker::get_Right()
extern void HandTracker_get_Right_mE982CD586A24562FF6DEFDA74A18F3F3CC8E4204 (void);
// 0x00000017 System.Void Microsoft.MixedReality.OpenXR.HandTracker::.ctor(Microsoft.MixedReality.OpenXR.Handedness)
extern void HandTracker__ctor_m241029B826E3D713B0A442C6B4AC6837BE7F79D5 (void);
// 0x00000018 System.Boolean Microsoft.MixedReality.OpenXR.HandTracker::TryLocateHandJoints(Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.HandJointLocation[])
extern void HandTracker_TryLocateHandJoints_m9E4B25E425CFBA340A2FAD9F8258148B95C7EA02 (void);
// 0x00000019 System.Void Microsoft.MixedReality.OpenXR.HandTracker::.cctor()
extern void HandTracker__cctor_m49B8E0D3ED8668067499B7CD682866BFB37379A0 (void);
// 0x0000001A System.Boolean Microsoft.MixedReality.OpenXR.HandJointLocation::get_IsTracked()
extern void HandJointLocation_get_IsTracked_m51143EF40DA6C6DED3F7F373D8DD95D1481536AC (void);
// 0x0000001B UnityEngine.Pose Microsoft.MixedReality.OpenXR.HandJointLocation::get_Pose()
extern void HandJointLocation_get_Pose_m2C026225983AF66BBFF803DA3B629AA60D7E7111 (void);
// 0x0000001C System.Single Microsoft.MixedReality.OpenXR.HandJointLocation::get_Radius()
extern void HandJointLocation_get_Radius_mC4680C1AB3CA80BB367A31F313506FD55634B07E (void);
// 0x0000001D Microsoft.MixedReality.OpenXR.OpenXRContext Microsoft.MixedReality.OpenXR.OpenXRContext::get_Current()
extern void OpenXRContext_get_Current_mBC400417833D9E04FC0E88EF0CDA1194498A17A4 (void);
// 0x0000001E System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRContext::get_Instance()
extern void OpenXRContext_get_Instance_mD074BAA20FDA19BEA6C912F1E719450618A21DD7 (void);
// 0x0000001F System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRContext::get_SystemId()
extern void OpenXRContext_get_SystemId_mA719169D4E7457DAC64FC0DC5F0988BD61AE3B3E (void);
// 0x00000020 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRContext::get_Session()
extern void OpenXRContext_get_Session_m16BAFDCA13AFB4EB2198FD94DAE016EB3F4ADACF (void);
// 0x00000021 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRContext::get_SceneOriginSpace()
extern void OpenXRContext_get_SceneOriginSpace_m603CB8956424839B946F26EAEFD2F5E38B058EDB (void);
// 0x00000022 System.IntPtr Microsoft.MixedReality.OpenXR.OpenXRContext::GetInstanceProcAddr(System.String)
extern void OpenXRContext_GetInstanceProcAddr_m5F67885D737B3C6CED1795819098FC36FD1E83E1 (void);
// 0x00000023 System.Void Microsoft.MixedReality.OpenXR.OpenXRContext::.ctor()
extern void OpenXRContext__ctor_m4641FAD672ED8D529977D80D88A12CA9E9AAF24F (void);
// 0x00000024 System.Void Microsoft.MixedReality.OpenXR.OpenXRContext::.cctor()
extern void OpenXRContext__cctor_m164E317D1062BDD454A8EC09619EC07A238922A8 (void);
// 0x00000025 System.Object Microsoft.MixedReality.OpenXR.PerceptionInterop::GetSceneCoordinateSystem(UnityEngine.Pose)
extern void PerceptionInterop_GetSceneCoordinateSystem_m807DA6DF91D533221B0657691E0EB79EFC7261B4 (void);
// 0x00000026 Microsoft.MixedReality.OpenXR.SpatialGraphNode Microsoft.MixedReality.OpenXR.SpatialGraphNode::FromStaticNodeId(System.Guid)
extern void SpatialGraphNode_FromStaticNodeId_mDF8748AE0B75507E1A079DE884AB078A32F8CEE8 (void);
// 0x00000027 System.Guid Microsoft.MixedReality.OpenXR.SpatialGraphNode::get_Id()
extern void SpatialGraphNode_get_Id_mC1FDA98FA1CE11089B6B0EB88103F7102D3EE290 (void);
// 0x00000028 System.Void Microsoft.MixedReality.OpenXR.SpatialGraphNode::set_Id(System.Guid)
extern void SpatialGraphNode_set_Id_m858DAB91CFA258872C5BFC4A24B8961CEF717F6C (void);
// 0x00000029 System.Boolean Microsoft.MixedReality.OpenXR.SpatialGraphNode::TryLocate(Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Pose&)
extern void SpatialGraphNode_TryLocate_mDEE58FCA71AAA792D9416859990508ACCAD83639 (void);
// 0x0000002A System.Void Microsoft.MixedReality.OpenXR.SpatialGraphNode::.ctor()
extern void SpatialGraphNode__ctor_m5C79E9D0299FD6E01CBBA14EE6E724166AADC30C (void);
// 0x0000002B System.Collections.Generic.IReadOnlyList`1<System.String> Microsoft.MixedReality.OpenXR.XRAnchorStore::get_PersistedAnchorNames()
extern void XRAnchorStore_get_PersistedAnchorNames_m02DF5BF59BAA8481421A39912C00907F3DECD57A (void);
// 0x0000002C UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.XRAnchorStore::LoadAnchor(System.String)
extern void XRAnchorStore_LoadAnchor_m382EC2C2A2E7585812012AB31EB3C8CAFAC31543 (void);
// 0x0000002D System.Boolean Microsoft.MixedReality.OpenXR.XRAnchorStore::TryPersistAnchor(UnityEngine.XR.ARSubsystems.TrackableId,System.String)
extern void XRAnchorStore_TryPersistAnchor_m797A5EF701F46A945E971C08D3F0042A45E1F8D9 (void);
// 0x0000002E System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore::UnpersistAnchor(System.String)
extern void XRAnchorStore_UnpersistAnchor_m9A67DF83DE5DFFE3D429C3789EC51096A787A391 (void);
// 0x0000002F System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore::Clear()
extern void XRAnchorStore_Clear_m9A9896E041D711F0129AFCAC6389F700F13F920B (void);
// 0x00000030 System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.XRAnchorStore> Microsoft.MixedReality.OpenXR.XRAnchorStore::LoadAsync(UnityEngine.XR.ARSubsystems.XRAnchorSubsystem)
extern void XRAnchorStore_LoadAsync_m131CC59FFEC0089EE80806DD920216738615FA53 (void);
// 0x00000031 System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore::.ctor(Microsoft.MixedReality.OpenXR.OpenXRAnchorStore)
extern void XRAnchorStore__ctor_m44F4DB82058C0EED1A420D27412D77F29F7AB572 (void);
// 0x00000032 System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore/<LoadAsync>d__6::MoveNext()
extern void U3CLoadAsyncU3Ed__6_MoveNext_mBCF49E09B452975FEDE05040DA5B235970426A29 (void);
// 0x00000033 System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore/<LoadAsync>d__6::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadAsyncU3Ed__6_SetStateMachine_m534F081E0324AE9975FFCD32E12481754FBC0273 (void);
// 0x00000034 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile::RegisterDeviceLayout()
extern void HPMixedRealityControllerProfile_RegisterDeviceLayout_m49274350FF54AD86B5D382CC6E6649238C9A1DD5 (void);
// 0x00000035 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile::UnregisterDeviceLayout()
extern void HPMixedRealityControllerProfile_UnregisterDeviceLayout_m0D9C3D496308DAD86185939D72CB3DD213484715 (void);
// 0x00000036 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile::RegisterActionMapsWithRuntime()
extern void HPMixedRealityControllerProfile_RegisterActionMapsWithRuntime_m0AF3FCE126C6DDEB5A57BF322223D4C695BB6E11 (void);
// 0x00000037 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile::.ctor()
extern void HPMixedRealityControllerProfile__ctor_mD8225AE3A8E0E0D34AEA23FFD456C5114B6DD593 (void);
// 0x00000038 UnityEngine.InputSystem.Controls.Vector2Control Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_thumbstick()
extern void HPMixedRealityController_get_thumbstick_mAF596F5D62FD1390ED84F2263868A9A580D36A19 (void);
// 0x00000039 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_thumbstick(UnityEngine.InputSystem.Controls.Vector2Control)
extern void HPMixedRealityController_set_thumbstick_mFD2CD57FE1FD155BF3B286C8D0F8FB0E203CE3BE (void);
// 0x0000003A UnityEngine.InputSystem.Controls.AxisControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_grip()
extern void HPMixedRealityController_get_grip_mDA28DE2D14CCC7504883B37A93514FB43F006FB6 (void);
// 0x0000003B System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_grip(UnityEngine.InputSystem.Controls.AxisControl)
extern void HPMixedRealityController_set_grip_mC88AF6CA565C706ABA83DD8B8880CEBAF4B868B7 (void);
// 0x0000003C UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_gripPressed()
extern void HPMixedRealityController_get_gripPressed_mCE729C9ADAE41D19428F8E2BBF40588FF948F0D5 (void);
// 0x0000003D System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_gripPressed(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_gripPressed_m364B829EB8E29353D0839FE829F062E3BB369605 (void);
// 0x0000003E UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_menu()
extern void HPMixedRealityController_get_menu_m5C4FF39AA772655F5AC903E37E7B8061404331A2 (void);
// 0x0000003F System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_menu(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_menu_m7D4B9F47F951C23F3FCB770A086F24B3A4FE4CE2 (void);
// 0x00000040 UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_primaryButton()
extern void HPMixedRealityController_get_primaryButton_mC1D79ECAEB20F71E9B6F62DE62FB91234BAC7E00 (void);
// 0x00000041 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_primaryButton(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_primaryButton_mD59092E0F3B3C3C06F8A5E303EBB38AB7062125A (void);
// 0x00000042 UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_secondaryButton()
extern void HPMixedRealityController_get_secondaryButton_m522E013F2392D3757CAB1C2E50F1372E794A2BF0 (void);
// 0x00000043 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_secondaryButton(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_secondaryButton_mEDE4578A0D487BFC20A11E0E2080EFA71D9EC32B (void);
// 0x00000044 UnityEngine.InputSystem.Controls.AxisControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_trigger()
extern void HPMixedRealityController_get_trigger_m3C82D2CD2CF75A793CFB157D7D539435E995796F (void);
// 0x00000045 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_trigger(UnityEngine.InputSystem.Controls.AxisControl)
extern void HPMixedRealityController_set_trigger_mE22DBF7D0746913CB1389B77755C7BDEAA86F731 (void);
// 0x00000046 UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_triggerPressed()
extern void HPMixedRealityController_get_triggerPressed_m829FF31A20A1477779679C5465FA7E95E192AEF0 (void);
// 0x00000047 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_triggerPressed(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_triggerPressed_m22920651BC5028CAA6636F27E079F114C2CAA473 (void);
// 0x00000048 UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_thumbstickClicked()
extern void HPMixedRealityController_get_thumbstickClicked_mE2D40DA9F3A148B63371A669F1FB0E2BE0FBA088 (void);
// 0x00000049 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_thumbstickClicked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_thumbstickClicked_mC7954B87789BCFF677D4332695E066D98302E984 (void);
// 0x0000004A UnityEngine.XR.OpenXR.Input.PoseControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_devicePose()
extern void HPMixedRealityController_get_devicePose_mF7592F1128776F5EABD6C606A551F35E0C4AFB83 (void);
// 0x0000004B System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_devicePose(UnityEngine.XR.OpenXR.Input.PoseControl)
extern void HPMixedRealityController_set_devicePose_mC299B0AA214C1B230C6B4E2E49C782661B774A14 (void);
// 0x0000004C UnityEngine.XR.OpenXR.Input.PoseControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_pointer()
extern void HPMixedRealityController_get_pointer_m58656C86EB6BB9102FFCB2E3389E87FDD807387A (void);
// 0x0000004D System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_pointer(UnityEngine.XR.OpenXR.Input.PoseControl)
extern void HPMixedRealityController_set_pointer_m3FECB2C9ABE6361BB9ABF806E2956378ACE1307F (void);
// 0x0000004E UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_isTracked()
extern void HPMixedRealityController_get_isTracked_mBD28ABE710C0905186B66CDE147EA89C9697BD64 (void);
// 0x0000004F System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_isTracked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_isTracked_mD1F14D7477C99032DB19F2B1817CF9BF1145A275 (void);
// 0x00000050 UnityEngine.InputSystem.Controls.IntegerControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_trackingState()
extern void HPMixedRealityController_get_trackingState_m2511F52A177FEBC517581F4E16FB2959D67696F1 (void);
// 0x00000051 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_trackingState(UnityEngine.InputSystem.Controls.IntegerControl)
extern void HPMixedRealityController_set_trackingState_mB0388A11FBC834711BD27C9AFEA51E0614F10094 (void);
// 0x00000052 UnityEngine.InputSystem.Controls.Vector3Control Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_devicePosition()
extern void HPMixedRealityController_get_devicePosition_mFA54918275B1ADDC2A82956E02603085AE216FC2 (void);
// 0x00000053 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_devicePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void HPMixedRealityController_set_devicePosition_m5120A8C4D41DAF34EEE3DA83B26D12D1793F0876 (void);
// 0x00000054 UnityEngine.InputSystem.Controls.QuaternionControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_deviceRotation()
extern void HPMixedRealityController_get_deviceRotation_mC71365652898A79EB1B85607C7C7A8B4E3833B65 (void);
// 0x00000055 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_deviceRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void HPMixedRealityController_set_deviceRotation_m4F2BC75708DF0E4BBE8E9859A903B737A50478D8 (void);
// 0x00000056 UnityEngine.InputSystem.Controls.Vector3Control Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_pointerPosition()
extern void HPMixedRealityController_get_pointerPosition_m36ED21C8D9F39811ECB2A29292D1BA63B4E35BD1 (void);
// 0x00000057 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_pointerPosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void HPMixedRealityController_set_pointerPosition_mC9565E44CBDDBA5A1EBF814A9CA46CCD1A2E75B6 (void);
// 0x00000058 UnityEngine.InputSystem.Controls.QuaternionControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_pointerRotation()
extern void HPMixedRealityController_get_pointerRotation_m1B6AFD80F6AD67C95F6EF6DC3EC62ECDB26CAFE2 (void);
// 0x00000059 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_pointerRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void HPMixedRealityController_set_pointerRotation_mB57963FF437CB7D49EE5BFD76BC4F2F76CFF1942 (void);
// 0x0000005A System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::FinishSetup()
extern void HPMixedRealityController_FinishSetup_m74DB304BBC63A2B04E8D68619D527364C224140F (void);
// 0x0000005B System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::.ctor()
extern void HPMixedRealityController__ctor_m9E53BEC36F0F78D75E3D75B1B0923B1B38F0374D (void);
// 0x0000005C System.Void Microsoft.MixedReality.OpenXR.HandTrackingFeaturePlugin::.ctor()
extern void HandTrackingFeaturePlugin__ctor_m271EA4AB37E57E2F8A8C3E97AF0DFA2210D472CC (void);
// 0x0000005D System.Void Microsoft.MixedReality.OpenXR.HoloLensFeaturePlugin::.ctor()
extern void HoloLensFeaturePlugin__ctor_m46DE918A938970EF87CE6CCF3987275146403454 (void);
// 0x0000005E System.IntPtr Microsoft.MixedReality.OpenXR.HoloLensFeaturePlugin::TryAcquireSceneCoordinateSystem(UnityEngine.Pose)
extern void HoloLensFeaturePlugin_TryAcquireSceneCoordinateSystem_m3C10BA132FF399720A2E3BFC71AD61541BB7839C (void);
// 0x0000005F System.IntPtr Microsoft.MixedReality.OpenXR.HoloLensFeaturePlugin::TryAcquirePerceptionSpatialAnchor(System.UInt64)
extern void HoloLensFeaturePlugin_TryAcquirePerceptionSpatialAnchor_mB750E3B92A2A80460104D812486F10140D70223F (void);
// 0x00000060 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_Instance()
// 0x00000061 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_Instance(System.UInt64)
// 0x00000062 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_SystemId()
// 0x00000063 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_SystemId(System.UInt64)
// 0x00000064 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_Session()
// 0x00000065 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_Session(System.UInt64)
// 0x00000066 Microsoft.MixedReality.OpenXR.XrSessionState Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_SessionState()
// 0x00000067 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_SessionState(Microsoft.MixedReality.OpenXR.XrSessionState)
// 0x00000068 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_SceneOriginSpace()
// 0x00000069 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_SceneOriginSpace(System.UInt64)
// 0x0000006A System.Boolean Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_IsAnchorExtensionSupported()
// 0x0000006B System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_IsAnchorExtensionSupported(System.Boolean)
// 0x0000006C System.IntPtr Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::GetInstanceProcAddr(System.String)
// 0x0000006D System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::.cctor()
// 0x0000006E System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::.ctor()
// 0x0000006F System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::AddSubsystemController(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x00000070 System.Boolean Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::IsExtensionEnabled(System.String,System.UInt32)
// 0x00000071 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSubsystemCreate()
// 0x00000072 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSubsystemStart()
// 0x00000073 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSubsystemStop()
// 0x00000074 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSubsystemDestroy()
// 0x00000075 System.IntPtr Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::HookGetInstanceProcAddr(System.IntPtr)
// 0x00000076 System.Boolean Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnInstanceCreate(System.UInt64)
// 0x00000077 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnInstanceDestroy(System.UInt64)
// 0x00000078 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSystemChange(System.UInt64)
// 0x00000079 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionCreate(System.UInt64)
// 0x0000007A System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionBegin(System.UInt64)
// 0x0000007B System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionStateChange(System.Int32,System.Int32)
// 0x0000007C System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionEnd(System.UInt64)
// 0x0000007D System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionDestroy(System.UInt64)
// 0x0000007E System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnAppSpaceChange(System.UInt64)
// 0x0000007F System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::Microsoft.MixedReality.OpenXR.ISubsystemPlugin.CreateSubsystem(System.Collections.Generic.List`1<TDescriptor>,System.String)
// 0x00000080 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::Microsoft.MixedReality.OpenXR.ISubsystemPlugin.StartSubsystem()
// 0x00000081 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::Microsoft.MixedReality.OpenXR.ISubsystemPlugin.StopSubsystem()
// 0x00000082 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::Microsoft.MixedReality.OpenXR.ISubsystemPlugin.DestroySubsystem()
// 0x00000083 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::<OnSubsystemCreate>b__31_0(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x00000084 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::<OnSubsystemStart>b__32_0(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x00000085 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::<OnSubsystemStop>b__33_0(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x00000086 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::<OnSubsystemDestroy>b__34_0(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x00000087 UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.FeatureUtils::ToTrackableId(System.Guid)
extern void FeatureUtils_ToTrackableId_m7A0DF91C9E58B6E7FD8D46A1857A817FE9655862 (void);
// 0x00000088 System.Guid Microsoft.MixedReality.OpenXR.FeatureUtils::ToGuid(UnityEngine.XR.ARSubsystems.TrackableId)
extern void FeatureUtils_ToGuid_m65B1FC59E6FA5CEF419854060A90B7DE8F93A006 (void);
// 0x00000089 Microsoft.MixedReality.OpenXR.NativeLibToken Microsoft.MixedReality.OpenXR.NativeLibTokenAttribute::get_NativeLibToken()
extern void NativeLibTokenAttribute_get_NativeLibToken_m1E0D4133836DA7016A8CDC0D1C1FAA88E5B449CA (void);
// 0x0000008A System.Void Microsoft.MixedReality.OpenXR.NativeLibTokenAttribute::set_NativeLibToken(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLibTokenAttribute_set_NativeLibToken_mA68CB70DF5B14C74A69AD1E158B81F016B4B928F (void);
// 0x0000008B System.Void Microsoft.MixedReality.OpenXR.NativeLibTokenAttribute::.ctor()
extern void NativeLibTokenAttribute__ctor_m3133A06535E087EF597350974028CB3B5047ED18 (void);
// 0x0000008C System.Void Microsoft.MixedReality.OpenXR.NativeLib::InitializeNativeLibToken(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_InitializeNativeLibToken_m81257382A6C069BCEA9827E59615EA1BD9DAD69D (void);
// 0x0000008D System.IntPtr Microsoft.MixedReality.OpenXR.NativeLib::HookGetInstanceProcAddr(Microsoft.MixedReality.OpenXR.NativeLibToken,System.IntPtr)
extern void NativeLib_HookGetInstanceProcAddr_mD4463C5A2273A46471E31CC473C7182766ACA679 (void);
// 0x0000008E System.IntPtr Microsoft.MixedReality.OpenXR.NativeLib::GetInstanceProcAddr(System.UInt64,System.IntPtr,System.String)
extern void NativeLib_GetInstanceProcAddr_m84620E93A762D8FBA8D0E1C048552CF45C8F583D (void);
// 0x0000008F System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetXrInstance(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_SetXrInstance_m83F1429AA035535FB64EE7A2043D20ED8B616390 (void);
// 0x00000090 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetXrSystemId(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_SetXrSystemId_m012911CE48E4D46715E49671078E582CBD9CA5B4 (void);
// 0x00000091 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetXrSession(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_SetXrSession_mBC371F3F4E6FB2E2EA346B0BF9C11A2AE3493302 (void);
// 0x00000092 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetXrSessionRunning(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Boolean)
extern void NativeLib_SetXrSessionRunning_mD14F928350AB73622DD2701E54F0162849B4F4BF (void);
// 0x00000093 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetSessionState(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt32)
extern void NativeLib_SetSessionState_m752D96C4C84A682BEAAE33A1E94E9159D18976BB (void);
// 0x00000094 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetSceneOriginSpace(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_SetSceneOriginSpace_mCD499B60B7F81481A9A522D4DC11560785462889 (void);
// 0x00000095 System.Void Microsoft.MixedReality.OpenXR.NativeLib::CreateSceneUnderstandingProvider(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_CreateSceneUnderstandingProvider_m24121E24AABDC8ED606D92A34BCD30D6325AEE0D (void);
// 0x00000096 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetSceneUnderstandingActive(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Boolean)
extern void NativeLib_SetSceneUnderstandingActive_m950430F0454632292CEA18AC22752918D5105029 (void);
// 0x00000097 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetPlaneDetectionMode(Microsoft.MixedReality.OpenXR.NativeLibToken,UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void NativeLib_SetPlaneDetectionMode_m49EA191D8BCA0EFCACA42B43F7480616FE5EA3A0 (void);
// 0x00000098 System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetNumPlaneChanges(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.FrameTime,System.UInt32&,System.UInt32&,System.UInt32&)
extern void NativeLib_GetNumPlaneChanges_m54B42731A229E4E6043444DF047BCEA9721D52DC (void);
// 0x00000099 System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetPlaneChanges(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Void*,System.Void*,System.Void*)
extern void NativeLib_GetPlaneChanges_mDAB819014D611F2EFF5A1240847552AFF19AB4A0 (void);
// 0x0000009A System.Void Microsoft.MixedReality.OpenXR.NativeLib::CreateAnchorProvider(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_CreateAnchorProvider_mD2094B903F5B800505134C035A52D9CEC3AA8331 (void);
// 0x0000009B System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryAddAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Quaternion,UnityEngine.Vector3,System.Void*)
extern void NativeLib_TryAddAnchor_m4F5F1B79B8A82459BA41F3E9A31A287B45B75093 (void);
// 0x0000009C System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryRemoveAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Guid)
extern void NativeLib_TryRemoveAnchor_m053AEA4308FB0237BE6FEE3C20F466C05EA09B16 (void);
// 0x0000009D System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetNumAnchorChanges(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.FrameTime,System.UInt32&,System.UInt32&,System.UInt32&)
extern void NativeLib_GetNumAnchorChanges_m99E1399286E46F015FF40A6F2819BC9BFF1DEEF3 (void);
// 0x0000009E System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetAnchorChanges(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Void*,System.Void*,System.Void*)
extern void NativeLib_GetAnchorChanges_m549F1D7B4EF421FCAEE99184BD19C29752CF9E71 (void);
// 0x0000009F System.Void Microsoft.MixedReality.OpenXR.NativeLib::LoadAnchorStore(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_LoadAnchorStore_mB6B782526F92CA3D86028AE5495D6F3E25973819 (void);
// 0x000000A0 System.UInt32 Microsoft.MixedReality.OpenXR.NativeLib::GetNumPersistedAnchorNames(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_GetNumPersistedAnchorNames_mA4438EB88A6B6841FDFD89F2A319279875169A44 (void);
// 0x000000A1 System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetPersistedAnchorName(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void NativeLib_GetPersistedAnchorName_mF25E5A0F7B926E2F4F4B591B19B092715EF04E4C (void);
// 0x000000A2 System.Guid Microsoft.MixedReality.OpenXR.NativeLib::LoadPersistedAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.String)
extern void NativeLib_LoadPersistedAnchor_mCCCA9BAAF0607671A1E59B4742067EEC1F22CACA (void);
// 0x000000A3 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryPersistAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.String,System.Guid)
extern void NativeLib_TryPersistAnchor_m0DECB562CC141274520CCB246C7721D7C15F7C7C (void);
// 0x000000A4 System.Void Microsoft.MixedReality.OpenXR.NativeLib::UnpersistAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.String)
extern void NativeLib_UnpersistAnchor_m402B9AFA1DD1DD159C82D7EAFA2B9793054DF007 (void);
// 0x000000A5 System.Void Microsoft.MixedReality.OpenXR.NativeLib::ClearPersistedAnchors(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_ClearPersistedAnchors_m80A84716788D0EE6F9F146FFC23A917F59909DC1 (void);
// 0x000000A6 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::GetHandJointData(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Handedness,Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.HandJointLocation[])
extern void NativeLib_GetHandJointData_mBD6CABEA9ACE681D7F2AAF7E4EC075B6A365475B (void);
// 0x000000A7 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryLocateHandMesh(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Handedness,Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.HandPoseType,UnityEngine.Pose&)
extern void NativeLib_TryLocateHandMesh_m44907E709723D0B06F5036D0799A11320AE24118 (void);
// 0x000000A8 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryGetHandMesh(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Handedness,Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.HandPoseType,System.UInt64&,System.Int32&,UnityEngine.Vector3[],UnityEngine.Vector3[],System.UInt32&,System.Int32&,System.Int32[])
extern void NativeLib_TryGetHandMesh_m3E1D62053260FAE972CA62016C78C2A42387428A (void);
// 0x000000A9 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryGetHandMeshBufferSizes(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt32&,System.UInt32&)
extern void NativeLib_TryGetHandMeshBufferSizes_mC6128E4F2C30352D96E039A7CA070E2B15194925 (void);
// 0x000000AA System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryEnableRemotingOverride(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_TryEnableRemotingOverride_m29AEEFA1871D473D2745DB94D7399B8569B63593 (void);
// 0x000000AB System.Void Microsoft.MixedReality.OpenXR.NativeLib::ResetRemotingOverride(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_ResetRemotingOverride_mC66AC58D7FD6E20216C01D70E6C991009025E89A (void);
// 0x000000AC System.Void Microsoft.MixedReality.OpenXR.NativeLib::ConnectRemoting(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Remoting.RemotingConfiguration)
extern void NativeLib_ConnectRemoting_mD0C56272108E10664423548F4FFAA4E4B63334B1 (void);
// 0x000000AD System.Void Microsoft.MixedReality.OpenXR.NativeLib::DisconnectRemoting(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_DisconnectRemoting_m5F2D5C74C51DF89C24B217D8FC5D465B5C66B22F (void);
// 0x000000AE System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryGetRemotingConnectionState(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Remoting.ConnectionState&,Microsoft.MixedReality.OpenXR.Remoting.DisconnectReason&)
extern void NativeLib_TryGetRemotingConnectionState_m1D38B9DE045B2FFAC3C09E7F705D6CEDC4EF3D0D (void);
// 0x000000AF System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryCreateSpaceFromStaticNodeId(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Guid,System.UInt64&)
extern void NativeLib_TryCreateSpaceFromStaticNodeId_m01A69A41D7E1A4018C2ECAF0ECE69A2E5205F784 (void);
// 0x000000B0 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryLocateSpace(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64,Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Pose&)
extern void NativeLib_TryLocateSpace_mAE1D17C66EFC2CF6454746B8F8C195FB96D402C1 (void);
// 0x000000B1 System.IntPtr Microsoft.MixedReality.OpenXR.NativeLib::TryAcquireSceneCoordinateSystem(Microsoft.MixedReality.OpenXR.NativeLibToken,UnityEngine.Pose)
extern void NativeLib_TryAcquireSceneCoordinateSystem_mA3177E8BB3AD6B10E639BC175C39283A6BA918D3 (void);
// 0x000000B2 System.IntPtr Microsoft.MixedReality.OpenXR.NativeLib::TryAcquirePerceptionSpatialAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_TryAcquirePerceptionSpatialAnchor_m9B50DC084DD34CD699689AA7FA60AA0F92C33056 (void);
// 0x000000B3 System.Void Microsoft.MixedReality.OpenXR.NativeLib::.ctor()
extern void NativeLib__ctor_m41347442DBD619986F4E901D741DA1A41A35946C (void);
// 0x000000B4 System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.OpenXRAnchorStore> Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory::LoadAnchorStoreAsync(UnityEngine.XR.ARSubsystems.XRAnchorSubsystem)
extern void OpenXRAnchorStoreFactory_LoadAnchorStoreAsync_m12D4957E452A9F50845E9ABCCF30F0847CA2DBFE (void);
// 0x000000B5 System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory::.cctor()
extern void OpenXRAnchorStoreFactory__cctor_mCDDDC2539476B73F8557CA5565D20577415A3866 (void);
// 0x000000B6 System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory/<>c::.cctor()
extern void U3CU3Ec__cctor_m56329537EFC168C4C4ACE319028572DDCF30F640 (void);
// 0x000000B7 System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory/<>c::.ctor()
extern void U3CU3Ec__ctor_mC78C4A92A9D067E2B47A16A9FD257A858E4F7667 (void);
// 0x000000B8 Microsoft.MixedReality.OpenXR.OpenXRAnchorStore Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory/<>c::<LoadAnchorStoreAsync>b__2_0()
extern void U3CU3Ec_U3CLoadAnchorStoreAsyncU3Eb__2_0_mE20122EC0C2727630F76DA3589B46EB94EE6DAB6 (void);
// 0x000000B9 System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::.ctor()
extern void OpenXRAnchorStore__ctor_mEB085F594A7E919BE29DAB959D5C83FFCE14B704 (void);
// 0x000000BA System.Collections.Generic.IReadOnlyList`1<System.String> Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::get_PersistedAnchorNames()
extern void OpenXRAnchorStore_get_PersistedAnchorNames_m4AAFD1C75A39A057709C557E3DEC1B48022BF745 (void);
// 0x000000BB System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::UpdatePersistedAnchorNames()
extern void OpenXRAnchorStore_UpdatePersistedAnchorNames_m4F69828BC42D615F2B4AD0F2E38932671979C132 (void);
// 0x000000BC UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::LoadAnchor(System.String)
extern void OpenXRAnchorStore_LoadAnchor_m198AD532CA4648DB96993CB18D06AF2B894B5F0B (void);
// 0x000000BD System.Boolean Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::TryPersistAnchor(System.String,UnityEngine.XR.ARSubsystems.TrackableId)
extern void OpenXRAnchorStore_TryPersistAnchor_m12ECD1E0183D3F69F7B14E323F2B26EEC305539F (void);
// 0x000000BE System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::UnpersistAnchor(System.String)
extern void OpenXRAnchorStore_UnpersistAnchor_m5AB186165AFD489F117BC42A074B0F05FD2C14DE (void);
// 0x000000BF System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::Clear()
extern void OpenXRAnchorStore_Clear_mF9EAA9B0323A45C7A03655C9EFCDAE1187B51184 (void);
// 0x000000C0 System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::.cctor()
extern void OpenXRAnchorStore__cctor_m6CD7165BF4EEBC0D3F75F08BC1A0DB38659A68F4 (void);
// 0x000000C1 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem::RegisterDescriptor()
extern void AnchorSubsystem_RegisterDescriptor_m05F13E3CC968276C9553A108272FB9A68A97D5F1 (void);
// 0x000000C2 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem::.ctor()
extern void AnchorSubsystem__ctor_mF50564F70BBE697D9F5A4E2D5556CA4ABD5147F0 (void);
// 0x000000C3 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::.ctor()
extern void OpenXRProvider__ctor_m8DC96C043F6972BC08A2D1B653C7B0EA74F73782 (void);
// 0x000000C4 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::Destroy()
extern void OpenXRProvider_Destroy_m91435B6B58043B9093D454409FFCDC52D264E69D (void);
// 0x000000C5 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::Start()
extern void OpenXRProvider_Start_m07D518C964D7CF7BDBD8515FDEE29E79F7188480 (void);
// 0x000000C6 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::Stop()
extern void OpenXRProvider_Stop_mF8D132C1EB8EB78F38A8585D62B8408E87BBC665 (void);
// 0x000000C7 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRAnchor> Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::GetChanges(UnityEngine.XR.ARSubsystems.XRAnchor,Unity.Collections.Allocator)
extern void OpenXRProvider_GetChanges_m514C0F27748A988E0DB0FE727A4F5541B168628E (void);
// 0x000000C8 UnityEngine.XR.ARSubsystems.XRAnchor Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::ToXRAnchor(Microsoft.MixedReality.OpenXR.NativeAnchor)
extern void OpenXRProvider_ToXRAnchor_m6F4AC3C2D981AFFCAACB0865FBD1EBAFFB80ED00 (void);
// 0x000000C9 System.Boolean Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::TryAddAnchor(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void OpenXRProvider_TryAddAnchor_m8302C7C57F5D1D1CA341807033B43E89E397D9E0 (void);
// 0x000000CA System.Boolean Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::TryAttachAnchor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void OpenXRProvider_TryAttachAnchor_m52C56723C94868872BA1756ADFFE542E719C4A30 (void);
// 0x000000CB System.Boolean Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::TryRemoveAnchor(UnityEngine.XR.ARSubsystems.TrackableId)
extern void OpenXRProvider_TryRemoveAnchor_mB18B71C794BE19C9E8A252B8BC71FCED6B806ED0 (void);
// 0x000000CC System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void AnchorSubsystemController__ctor_m50CFFC6B09DCA86FBD7FB569480EDC9EBED763FC (void);
// 0x000000CD System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void AnchorSubsystemController_OnSubsystemCreate_m5509284C4C71533DE139537DB2020852050EDEA9 (void);
// 0x000000CE System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::OnSubsystemStart(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void AnchorSubsystemController_OnSubsystemStart_mB035923F3A90B46D42F2D9B402A59286EAFDDE2D (void);
// 0x000000CF System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::OnSubsystemStop(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void AnchorSubsystemController_OnSubsystemStop_m4CD2A808BF107D6A0E1FDBBA36E9AD4208E325EC (void);
// 0x000000D0 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void AnchorSubsystemController_OnSubsystemDestroy_m455A34ACC209D3A21D15EBAE97B315E4AF103DA0 (void);
// 0x000000D1 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::.cctor()
extern void AnchorSubsystemController__cctor_m15D0F86557975A471DAA760765E362C076152C02 (void);
// 0x000000D2 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void HandTrackingSubsystemController__ctor_mC2B8C8D115CD4623282BEEF8E31B03CDB4BBC992 (void);
// 0x000000D3 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void HandTrackingSubsystemController_OnSubsystemCreate_m8BFEB28514DD7F2265091D563DFC5FB6830CA9AF (void);
// 0x000000D4 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::OnSubsystemStart(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void HandTrackingSubsystemController_OnSubsystemStart_m8517B9E90CB1CD76989A5BF1DC0E3FFCBACFD8D4 (void);
// 0x000000D5 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::OnSubsystemStop(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void HandTrackingSubsystemController_OnSubsystemStop_mFA520313379300F894FA201B85D4182F97EB5FF7 (void);
// 0x000000D6 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void HandTrackingSubsystemController_OnSubsystemDestroy_m0A083F0821591236239A80400A549614FA53D9B3 (void);
// 0x000000D7 System.Void Microsoft.MixedReality.OpenXR.MeshSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void MeshSubsystemController__ctor_m18E6586F3FA5137B6FB112E493226698E2CAA876 (void);
// 0x000000D8 System.Void Microsoft.MixedReality.OpenXR.MeshSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void MeshSubsystemController_OnSubsystemCreate_m67776BA8F73289CFEC5D4F2D4EA3BC840F1BB144 (void);
// 0x000000D9 System.Void Microsoft.MixedReality.OpenXR.MeshSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void MeshSubsystemController_OnSubsystemDestroy_m193625592355574009F00261701A35153F7CDDA3 (void);
// 0x000000DA System.Void Microsoft.MixedReality.OpenXR.MeshSubsystemController::.cctor()
extern void MeshSubsystemController__cctor_m9115749A6A8A4503E8B4816C34D751BB42EFDA4B (void);
// 0x000000DB System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem::RegisterDescriptor()
extern void PlaneSubsystem_RegisterDescriptor_m7F301C3061BA767C73C3841743EAAF0F67694FF4 (void);
// 0x000000DC System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem::.ctor()
extern void PlaneSubsystem__ctor_m92D726C6B59917D31864546D4C724426A370801D (void);
// 0x000000DD System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::.ctor()
extern void OpenXRProvider__ctor_m0810D0FBB2D3DA67331CC85048163C92CD79223D (void);
// 0x000000DE System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::Start()
extern void OpenXRProvider_Start_m9A67B2444C4C054B680554E57476905D29B9057A (void);
// 0x000000DF System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::Stop()
extern void OpenXRProvider_Stop_m52979099FC5DFFCB69BE529886255CB125B9B592 (void);
// 0x000000E0 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::Destroy()
extern void OpenXRProvider_Destroy_m6A4F9A31143AB6758888C67998EC809E5AE9C71D (void);
// 0x000000E1 UnityEngine.XR.ARSubsystems.PlaneDetectionMode Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::get_currentPlaneDetectionMode()
extern void OpenXRProvider_get_currentPlaneDetectionMode_m5112A4030805746A3D8B1AF2804E4DCDBE466F40 (void);
// 0x000000E2 UnityEngine.XR.ARSubsystems.PlaneDetectionMode Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::get_requestedPlaneDetectionMode()
extern void OpenXRProvider_get_requestedPlaneDetectionMode_mED9D9BFC796B50B9D0981630C77842987272751E (void);
// 0x000000E3 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::set_requestedPlaneDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void OpenXRProvider_set_requestedPlaneDetectionMode_mC5FEA58C621235A65724016846BA9EA956650732 (void);
// 0x000000E4 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.BoundedPlane> Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::GetChanges(UnityEngine.XR.ARSubsystems.BoundedPlane,Unity.Collections.Allocator)
extern void OpenXRProvider_GetChanges_mF6B8282479E9F42AA40158953E617F599280E10C (void);
// 0x000000E5 UnityEngine.XR.ARSubsystems.PlaneClassification Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::ToPlaneClassification(Microsoft.MixedReality.OpenXR.XrSceneObjectKindMSFT)
extern void OpenXRProvider_ToPlaneClassification_m5013D8712913D88E1B8D08ACA9F4E28879FDE31D (void);
// 0x000000E6 UnityEngine.XR.ARSubsystems.BoundedPlane Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::ToBoundedPlane(Microsoft.MixedReality.OpenXR.NativePlane,UnityEngine.XR.ARSubsystems.BoundedPlane)
extern void OpenXRProvider_ToBoundedPlane_m4E4B463FC8D48BE21F61ACC518BB702912B55BE3 (void);
// 0x000000E7 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void PlaneSubsystemController__ctor_mDC113D59676EFA45A11F486B2A82FE44ECF6E744 (void);
// 0x000000E8 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void PlaneSubsystemController_OnSubsystemCreate_m2E658AB785FDF4C7CADC888C89593462BB34BA68 (void);
// 0x000000E9 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void PlaneSubsystemController_OnSubsystemDestroy_m0C3A0036BE5A59BEC8294643292084E69D10A32D (void);
// 0x000000EA System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystemController::.cctor()
extern void PlaneSubsystemController__cctor_m46A7EE7A7921D686BF5D536F6994513CF09877C3 (void);
// 0x000000EB System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystem::RegisterDescriptor()
extern void RaycastSubsystem_RegisterDescriptor_m9C781123128B093724F4EE7BD6CC91D962F398F3 (void);
// 0x000000EC System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystem::.ctor()
extern void RaycastSubsystem__ctor_mE9A9676881F16093A75F8EA0F3F3C144C934E78C (void);
// 0x000000ED System.Boolean Microsoft.MixedReality.OpenXR.RaycastSubsystem/OpenXRProvider::TryAddRaycast(UnityEngine.Vector2,System.Single,UnityEngine.XR.ARSubsystems.XRRaycast&)
extern void OpenXRProvider_TryAddRaycast_mAB13C8024EC6BD339D8CD770307311B0A5B2EEA0 (void);
// 0x000000EE System.Boolean Microsoft.MixedReality.OpenXR.RaycastSubsystem/OpenXRProvider::TryAddRaycast(UnityEngine.Ray,System.Single,UnityEngine.XR.ARSubsystems.XRRaycast&)
extern void OpenXRProvider_TryAddRaycast_m9758DFBF8D6F4837B16F02F0D96C8E2059257A05 (void);
// 0x000000EF System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystem/OpenXRProvider::RemoveRaycast(UnityEngine.XR.ARSubsystems.TrackableId)
extern void OpenXRProvider_RemoveRaycast_m64C3FB8BD3C61E0D2FFAEE6A46F4CFAED0326B01 (void);
// 0x000000F0 System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystem/OpenXRProvider::.ctor()
extern void OpenXRProvider__ctor_mABC309D451B3863AC74398410EB1CA0EBC5FFA83 (void);
// 0x000000F1 System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void RaycastSubsystemController__ctor_m18B861B52A60AB845CC3D7A4F40B4C263A65EB68 (void);
// 0x000000F2 System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void RaycastSubsystemController_OnSubsystemCreate_m7AB05789E754DD2864B9A1DFCE5C6CACD82C68E7 (void);
// 0x000000F3 System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void RaycastSubsystemController_OnSubsystemDestroy_m397DD6C9E32AC9CFE4407F949E1851C1A8E03581 (void);
// 0x000000F4 System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystemController::.cctor()
extern void RaycastSubsystemController__cctor_m5FC43F03D5C5D6E88E3E12225EDAFB3B0E64CFAB (void);
// 0x000000F5 System.UInt64 Microsoft.MixedReality.OpenXR.IOpenXRContext::get_Instance()
// 0x000000F6 System.UInt64 Microsoft.MixedReality.OpenXR.IOpenXRContext::get_SystemId()
// 0x000000F7 System.UInt64 Microsoft.MixedReality.OpenXR.IOpenXRContext::get_Session()
// 0x000000F8 Microsoft.MixedReality.OpenXR.XrSessionState Microsoft.MixedReality.OpenXR.IOpenXRContext::get_SessionState()
// 0x000000F9 System.UInt64 Microsoft.MixedReality.OpenXR.IOpenXRContext::get_SceneOriginSpace()
// 0x000000FA System.Boolean Microsoft.MixedReality.OpenXR.IOpenXRContext::get_IsAnchorExtensionSupported()
// 0x000000FB System.Void Microsoft.MixedReality.OpenXR.ISubsystemPlugin::CreateSubsystem(System.Collections.Generic.List`1<TDescriptor>,System.String)
// 0x000000FC System.Void Microsoft.MixedReality.OpenXR.ISubsystemPlugin::StartSubsystem()
// 0x000000FD System.Void Microsoft.MixedReality.OpenXR.ISubsystemPlugin::StopSubsystem()
// 0x000000FE System.Void Microsoft.MixedReality.OpenXR.ISubsystemPlugin::DestroySubsystem()
// 0x000000FF System.Void Microsoft.MixedReality.OpenXR.SubsystemController::.ctor(Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void SubsystemController__ctor_m15FB20C0E85F5CDF70282C8908100C8A7CD4ED4A (void);
// 0x00000100 System.Void Microsoft.MixedReality.OpenXR.SubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void SubsystemController_OnSubsystemCreate_m46A1F68EA94FF1B417B336B87A51E923F6C6B77E (void);
// 0x00000101 System.Void Microsoft.MixedReality.OpenXR.SubsystemController::OnSubsystemStart(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void SubsystemController_OnSubsystemStart_mDC204F22C801970C9338E443D8CC74687DC465DD (void);
// 0x00000102 System.Void Microsoft.MixedReality.OpenXR.SubsystemController::OnSubsystemStop(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void SubsystemController_OnSubsystemStop_mFC6CE5C9E2D60F6B6FED51D5109B45DE08D96FCE (void);
// 0x00000103 System.Void Microsoft.MixedReality.OpenXR.SubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void SubsystemController_OnSubsystemDestroy_mBCE41C6C7A0382BC11F39026909517A7612E09AD (void);
// 0x00000104 System.Void Microsoft.MixedReality.OpenXR.Preview.HandTracker::.ctor(Microsoft.MixedReality.OpenXR.Preview.Handedness,Microsoft.MixedReality.OpenXR.Preview.HandPoseType)
extern void HandTracker__ctor_mFC002E1312D55C5EDD3D10C9B4C238F73CF0C5B0 (void);
// 0x00000105 System.Boolean Microsoft.MixedReality.OpenXR.Preview.HandTracker::TryLocateHandJoints(Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.Preview.HandJointLocation[])
extern void HandTracker_TryLocateHandJoints_m8BD920DD5BE76143129A3859D17E5D2AA2C6989D (void);
// 0x00000106 Microsoft.MixedReality.OpenXR.Preview.PoseFlags Microsoft.MixedReality.OpenXR.Preview.HandJointLocation::get_PoseFlags()
extern void HandJointLocation_get_PoseFlags_m59BFC0160D28158C2C5E94643054886705832873 (void);
// 0x00000107 UnityEngine.Quaternion Microsoft.MixedReality.OpenXR.Preview.HandJointLocation::get_Rotation()
extern void HandJointLocation_get_Rotation_mA49A45860B0B0CA1AF450DCA3F6593DDAA941946 (void);
// 0x00000108 UnityEngine.Vector3 Microsoft.MixedReality.OpenXR.Preview.HandJointLocation::get_Position()
extern void HandJointLocation_get_Position_mC02E65EBA165EF118F6A9C99A47F94790F0225EB (void);
// 0x00000109 System.Single Microsoft.MixedReality.OpenXR.Preview.HandJointLocation::get_Radius()
extern void HandJointLocation_get_Radius_m593CA1C90F41132D1E3569965C9B53C73E29071F (void);
// 0x0000010A System.Void Microsoft.MixedReality.OpenXR.Preview.HandJointLocation::.ctor(Microsoft.MixedReality.OpenXR.HandJointLocation)
extern void HandJointLocation__ctor_m60ED7D572A93EF4F57208983FB3638E03B6E3F3A (void);
// 0x0000010B Microsoft.MixedReality.OpenXR.Remoting.RemotingConfiguration Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::get_Configuration()
extern void AppRemoting_get_Configuration_m986F5261E0B5DF5918670E4BDB303DD6BB6CE6BB (void);
// 0x0000010C System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::set_Configuration(Microsoft.MixedReality.OpenXR.Remoting.RemotingConfiguration)
extern void AppRemoting_set_Configuration_m330236A0A1624400D8C2B79055275A5DBCDDD080 (void);
// 0x0000010D System.Collections.IEnumerator Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::Connect(Microsoft.MixedReality.OpenXR.Remoting.RemotingConfiguration)
extern void AppRemoting_Connect_mFB5585D81BEF6162D314684C0C1BD5D92EEFA435 (void);
// 0x0000010E System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::Disconnect()
extern void AppRemoting_Disconnect_mAE0A81B850728822A9DEE75A00C297CCC906549D (void);
// 0x0000010F System.Boolean Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::TryGetConnectionState(Microsoft.MixedReality.OpenXR.Remoting.ConnectionState&,Microsoft.MixedReality.OpenXR.Remoting.DisconnectReason&)
extern void AppRemoting_TryGetConnectionState_mD917542223797465AA54EA39811C4910212348C3 (void);
// 0x00000110 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::.ctor(System.Int32)
extern void U3CConnectU3Ed__4__ctor_mE84343A49B0B3DC1D602D771D3E878C7DD5A06A8 (void);
// 0x00000111 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::System.IDisposable.Dispose()
extern void U3CConnectU3Ed__4_System_IDisposable_Dispose_m5B1C77D83F06076D97CA121CF6E69BF5E5EDFD31 (void);
// 0x00000112 System.Boolean Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::MoveNext()
extern void U3CConnectU3Ed__4_MoveNext_m1A8964EC7416B44B2AFE8F013DF193FACA85F273 (void);
// 0x00000113 System.Object Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CConnectU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBA0A91330F728EBA406ED19CB72A80CA118C0EF9 (void);
// 0x00000114 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::System.Collections.IEnumerator.Reset()
extern void U3CConnectU3Ed__4_System_Collections_IEnumerator_Reset_mEF91BB0D15F59540613C5DA826BA48CACCA2D8A5 (void);
// 0x00000115 System.Object Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CConnectU3Ed__4_System_Collections_IEnumerator_get_Current_m9CC5E737A39AABD0493115AF1F3D1614D9B2B5C3 (void);
// 0x00000116 System.IntPtr Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::HookGetInstanceProcAddr(System.IntPtr)
extern void AppRemotingPlugin_HookGetInstanceProcAddr_m31BB148E2D2D2A77FC2E6FC7C8CF2B91F070BD4C (void);
// 0x00000117 System.Boolean Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::OnInstanceCreate(System.UInt64)
extern void AppRemotingPlugin_OnInstanceCreate_mF97A486535A5E5F405DF63F7888E91365D52487E (void);
// 0x00000118 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::OnInstanceDestroy(System.UInt64)
extern void AppRemotingPlugin_OnInstanceDestroy_mD14D534C2F5077AA616D709C4405845E8DA0D5A1 (void);
// 0x00000119 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::OnSystemChange(System.UInt64)
extern void AppRemotingPlugin_OnSystemChange_m6F340CB1DCE89999073AD8C3E0E904964AF9B4EA (void);
// 0x0000011A System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::OnSessionStateChange(System.Int32,System.Int32)
extern void AppRemotingPlugin_OnSessionStateChange_m5822F18A4E025121C9758DACD16389310CCF2C74 (void);
// 0x0000011B System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::.ctor()
extern void AppRemotingPlugin__ctor_m4C9DD14B55D812BCB1F43656660647DD6FC4AAEF (void);
// 0x0000011C System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin/<>c::.cctor()
extern void U3CU3Ec__cctor_m9EB91B1FAF58CD4567692E52F701DCF00CFEA761 (void);
// 0x0000011D System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin/<>c::.ctor()
extern void U3CU3Ec__ctor_m914051A6AAEFA1757292D09D57152A3EA4B0C420 (void);
// 0x0000011E System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin/<>c::<OnSystemChange>b__8_0()
extern void U3CU3Ec_U3COnSystemChangeU3Eb__8_0_m2EEB89E0083203E1265A13BDD49C2A0DEEA4CCA5 (void);
// 0x0000011F System.IntPtr Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::HookGetInstanceProcAddr(System.IntPtr)
extern void EditorRemotingPlugin_HookGetInstanceProcAddr_mBC57B868CED9BE657F9409ECC892A87149E7EA39 (void);
// 0x00000120 System.Boolean Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::OnInstanceCreate(System.UInt64)
extern void EditorRemotingPlugin_OnInstanceCreate_mA85E46C417E846CF247097FE2935CC41377CE8F6 (void);
// 0x00000121 System.Void Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::OnInstanceDestroy(System.UInt64)
extern void EditorRemotingPlugin_OnInstanceDestroy_m70C271FE49E987000476C490C1BDB5B86D19142A (void);
// 0x00000122 System.Void Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::OnSystemChange(System.UInt64)
extern void EditorRemotingPlugin_OnSystemChange_m377B9FA110A7814B34F23D563574DF9BDFA638E7 (void);
// 0x00000123 System.Void Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::OnSessionStateChange(System.Int32,System.Int32)
extern void EditorRemotingPlugin_OnSessionStateChange_mB2369C3B0913F984762627E941578F807F021CEE (void);
// 0x00000124 System.Boolean Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::HasValidSettings()
extern void EditorRemotingPlugin_HasValidSettings_m6505DB3B86D285EE886805B6DDFD9BD0BFED8C02 (void);
// 0x00000125 System.Void Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::.ctor()
extern void EditorRemotingPlugin__ctor_mB9F1507DF72CDDF13B33531FFBB2C3C0C9B70D43 (void);
// 0x00000126 System.Void Microsoft.MixedReality.OpenXR.Remoting.EditorRemotingPlugin::<OnSystemChange>b__13_0()
extern void EditorRemotingPlugin_U3COnSystemChangeU3Eb__13_0_m476B42C14BD5D134898232591CEDCC6E41A629EE (void);
// 0x00000127 System.UInt64 Microsoft.MixedReality.OpenXR.ARFoundation.ARAnchorExtensions::GetOpenXRHandle(UnityEngine.XR.ARFoundation.ARAnchor)
extern void ARAnchorExtensions_GetOpenXRHandle_m62E3251ED977F25F3F3FCF2F154582ADFF01DE84 (void);
// 0x00000128 System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.XRAnchorStore> Microsoft.MixedReality.OpenXR.ARFoundation.AnchorManagerExtensions::LoadAnchorStoreAsync(UnityEngine.XR.ARFoundation.ARAnchorManager)
extern void AnchorManagerExtensions_LoadAnchorStoreAsync_mCF939DB55C69215007B47E334D92613B1B2864E0 (void);
// 0x00000129 System.UInt64 Microsoft.MixedReality.OpenXR.ARSubsystems.XRAnchorExtensions::GetOpenXRHandle(UnityEngine.XR.ARSubsystems.XRAnchor)
extern void XRAnchorExtensions_GetOpenXRHandle_mA35F8245608C8F50704657EC1A5CA8084FADE65B (void);
// 0x0000012A System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.XRAnchorStore> Microsoft.MixedReality.OpenXR.ARSubsystems.AnchorSubsystemExtensions::LoadAnchorStoreAsync(UnityEngine.XR.ARSubsystems.XRAnchorSubsystem)
extern void AnchorSubsystemExtensions_LoadAnchorStoreAsync_mFEF6B387788DE8DF416392F252252C19DD0C0367 (void);
static Il2CppMethodPointer s_methodPointers[298] = 
{
	AnchorSubsystemExtensions_LoadAnchorStoreAsync_m8873DC6588F53E4AE69655C59A89D21FA5C99240,
	XRAnchorStore_LoadAsync_mEAD20EBFFF7B1772B5425B291F795CF2EDCD4BD2,
	XRAnchorStore__ctor_m2F2798339F8F3C5C3BCCBEF2DCA5D90BAEA414FB,
	XRAnchorStore_TryPersistAnchor_m8E9A497BD02EA856586E1D6775164BA9AC63A7B0,
	U3CLoadAsyncU3Ed__0_MoveNext_mD07CA3DEB02D0D731328F87FAEEBDE336B391476,
	U3CLoadAsyncU3Ed__0_SetStateMachine_m02FDE1D534549B190ABD2C8C23D1AE181F847620,
	AnchorConverter_ToOpenXRHandle_m0DF0F055393DDAA5A4CD5AB1887EB66CF609B589,
	AnchorConverter_ToPerceptionSpatialAnchor_m3D2AFD16D67E9A618EA6AFBFBC0AF081CD2FC9F5,
	EyeLevelSceneOrigin_OnEnable_m6BCDA1FB726B403592863A994349A2981D7BF1F6,
	EyeLevelSceneOrigin_OnDisable_m86AA78502E55ED72AD3C26A2C54E831EED0C1059,
	EyeLevelSceneOrigin_XrInput_trackingOriginUpdated_m406EDC09F5C2F1F2D28FCE2D3B0D2D65D4BEABC3,
	EyeLevelSceneOrigin_EnsureSceneOriginAtEyeLevel_mE8D73598B12357982C49A7003670C499C95025C0,
	EyeLevelSceneOrigin_SetEyeLevelTrackingOriginMode_m271BC6E7A37A791FA92FDB012377488A08B412D8,
	EyeLevelSceneOrigin__ctor_mBF34B058AC130FFFAC4905DF4606902326EEDBA4,
	HandMeshTracker_get_Left_m03C9D512E0686B3332AAA1BBB00222B1EEBC4CE8,
	HandMeshTracker_get_Right_m75A2B30E198B61E3B45977819080BD7ED16BCEDE,
	HandMeshTracker__ctor_mF9F1B43E432F8623BE61321740BF30FD04CB700C,
	HandMeshTracker_TryLocateHandMesh_m1CF196C1E53899409F7A6C0CD87527A83AE10082,
	HandMeshTracker_TryGetHandMesh_m8BACC3586977F84E3B20B7E9FB7CC105EC477FB4,
	HandMeshTracker__cctor_mEE2DB60BBF234CEA5D9483474787B00D1EB9D843,
	HandTracker_get_Left_m78E03ADBE7621FBB3508DDC93687FB739E938344,
	HandTracker_get_Right_mE982CD586A24562FF6DEFDA74A18F3F3CC8E4204,
	HandTracker__ctor_m241029B826E3D713B0A442C6B4AC6837BE7F79D5,
	HandTracker_TryLocateHandJoints_m9E4B25E425CFBA340A2FAD9F8258148B95C7EA02,
	HandTracker__cctor_m49B8E0D3ED8668067499B7CD682866BFB37379A0,
	HandJointLocation_get_IsTracked_m51143EF40DA6C6DED3F7F373D8DD95D1481536AC,
	HandJointLocation_get_Pose_m2C026225983AF66BBFF803DA3B629AA60D7E7111,
	HandJointLocation_get_Radius_mC4680C1AB3CA80BB367A31F313506FD55634B07E,
	OpenXRContext_get_Current_mBC400417833D9E04FC0E88EF0CDA1194498A17A4,
	OpenXRContext_get_Instance_mD074BAA20FDA19BEA6C912F1E719450618A21DD7,
	OpenXRContext_get_SystemId_mA719169D4E7457DAC64FC0DC5F0988BD61AE3B3E,
	OpenXRContext_get_Session_m16BAFDCA13AFB4EB2198FD94DAE016EB3F4ADACF,
	OpenXRContext_get_SceneOriginSpace_m603CB8956424839B946F26EAEFD2F5E38B058EDB,
	OpenXRContext_GetInstanceProcAddr_m5F67885D737B3C6CED1795819098FC36FD1E83E1,
	OpenXRContext__ctor_m4641FAD672ED8D529977D80D88A12CA9E9AAF24F,
	OpenXRContext__cctor_m164E317D1062BDD454A8EC09619EC07A238922A8,
	PerceptionInterop_GetSceneCoordinateSystem_m807DA6DF91D533221B0657691E0EB79EFC7261B4,
	SpatialGraphNode_FromStaticNodeId_mDF8748AE0B75507E1A079DE884AB078A32F8CEE8,
	SpatialGraphNode_get_Id_mC1FDA98FA1CE11089B6B0EB88103F7102D3EE290,
	SpatialGraphNode_set_Id_m858DAB91CFA258872C5BFC4A24B8961CEF717F6C,
	SpatialGraphNode_TryLocate_mDEE58FCA71AAA792D9416859990508ACCAD83639,
	SpatialGraphNode__ctor_m5C79E9D0299FD6E01CBBA14EE6E724166AADC30C,
	XRAnchorStore_get_PersistedAnchorNames_m02DF5BF59BAA8481421A39912C00907F3DECD57A,
	XRAnchorStore_LoadAnchor_m382EC2C2A2E7585812012AB31EB3C8CAFAC31543,
	XRAnchorStore_TryPersistAnchor_m797A5EF701F46A945E971C08D3F0042A45E1F8D9,
	XRAnchorStore_UnpersistAnchor_m9A67DF83DE5DFFE3D429C3789EC51096A787A391,
	XRAnchorStore_Clear_m9A9896E041D711F0129AFCAC6389F700F13F920B,
	XRAnchorStore_LoadAsync_m131CC59FFEC0089EE80806DD920216738615FA53,
	XRAnchorStore__ctor_m44F4DB82058C0EED1A420D27412D77F29F7AB572,
	U3CLoadAsyncU3Ed__6_MoveNext_mBCF49E09B452975FEDE05040DA5B235970426A29,
	U3CLoadAsyncU3Ed__6_SetStateMachine_m534F081E0324AE9975FFCD32E12481754FBC0273,
	HPMixedRealityControllerProfile_RegisterDeviceLayout_m49274350FF54AD86B5D382CC6E6649238C9A1DD5,
	HPMixedRealityControllerProfile_UnregisterDeviceLayout_m0D9C3D496308DAD86185939D72CB3DD213484715,
	HPMixedRealityControllerProfile_RegisterActionMapsWithRuntime_m0AF3FCE126C6DDEB5A57BF322223D4C695BB6E11,
	HPMixedRealityControllerProfile__ctor_mD8225AE3A8E0E0D34AEA23FFD456C5114B6DD593,
	HPMixedRealityController_get_thumbstick_mAF596F5D62FD1390ED84F2263868A9A580D36A19,
	HPMixedRealityController_set_thumbstick_mFD2CD57FE1FD155BF3B286C8D0F8FB0E203CE3BE,
	HPMixedRealityController_get_grip_mDA28DE2D14CCC7504883B37A93514FB43F006FB6,
	HPMixedRealityController_set_grip_mC88AF6CA565C706ABA83DD8B8880CEBAF4B868B7,
	HPMixedRealityController_get_gripPressed_mCE729C9ADAE41D19428F8E2BBF40588FF948F0D5,
	HPMixedRealityController_set_gripPressed_m364B829EB8E29353D0839FE829F062E3BB369605,
	HPMixedRealityController_get_menu_m5C4FF39AA772655F5AC903E37E7B8061404331A2,
	HPMixedRealityController_set_menu_m7D4B9F47F951C23F3FCB770A086F24B3A4FE4CE2,
	HPMixedRealityController_get_primaryButton_mC1D79ECAEB20F71E9B6F62DE62FB91234BAC7E00,
	HPMixedRealityController_set_primaryButton_mD59092E0F3B3C3C06F8A5E303EBB38AB7062125A,
	HPMixedRealityController_get_secondaryButton_m522E013F2392D3757CAB1C2E50F1372E794A2BF0,
	HPMixedRealityController_set_secondaryButton_mEDE4578A0D487BFC20A11E0E2080EFA71D9EC32B,
	HPMixedRealityController_get_trigger_m3C82D2CD2CF75A793CFB157D7D539435E995796F,
	HPMixedRealityController_set_trigger_mE22DBF7D0746913CB1389B77755C7BDEAA86F731,
	HPMixedRealityController_get_triggerPressed_m829FF31A20A1477779679C5465FA7E95E192AEF0,
	HPMixedRealityController_set_triggerPressed_m22920651BC5028CAA6636F27E079F114C2CAA473,
	HPMixedRealityController_get_thumbstickClicked_mE2D40DA9F3A148B63371A669F1FB0E2BE0FBA088,
	HPMixedRealityController_set_thumbstickClicked_mC7954B87789BCFF677D4332695E066D98302E984,
	HPMixedRealityController_get_devicePose_mF7592F1128776F5EABD6C606A551F35E0C4AFB83,
	HPMixedRealityController_set_devicePose_mC299B0AA214C1B230C6B4E2E49C782661B774A14,
	HPMixedRealityController_get_pointer_m58656C86EB6BB9102FFCB2E3389E87FDD807387A,
	HPMixedRealityController_set_pointer_m3FECB2C9ABE6361BB9ABF806E2956378ACE1307F,
	HPMixedRealityController_get_isTracked_mBD28ABE710C0905186B66CDE147EA89C9697BD64,
	HPMixedRealityController_set_isTracked_mD1F14D7477C99032DB19F2B1817CF9BF1145A275,
	HPMixedRealityController_get_trackingState_m2511F52A177FEBC517581F4E16FB2959D67696F1,
	HPMixedRealityController_set_trackingState_mB0388A11FBC834711BD27C9AFEA51E0614F10094,
	HPMixedRealityController_get_devicePosition_mFA54918275B1ADDC2A82956E02603085AE216FC2,
	HPMixedRealityController_set_devicePosition_m5120A8C4D41DAF34EEE3DA83B26D12D1793F0876,
	HPMixedRealityController_get_deviceRotation_mC71365652898A79EB1B85607C7C7A8B4E3833B65,
	HPMixedRealityController_set_deviceRotation_m4F2BC75708DF0E4BBE8E9859A903B737A50478D8,
	HPMixedRealityController_get_pointerPosition_m36ED21C8D9F39811ECB2A29292D1BA63B4E35BD1,
	HPMixedRealityController_set_pointerPosition_mC9565E44CBDDBA5A1EBF814A9CA46CCD1A2E75B6,
	HPMixedRealityController_get_pointerRotation_m1B6AFD80F6AD67C95F6EF6DC3EC62ECDB26CAFE2,
	HPMixedRealityController_set_pointerRotation_mB57963FF437CB7D49EE5BFD76BC4F2F76CFF1942,
	HPMixedRealityController_FinishSetup_m74DB304BBC63A2B04E8D68619D527364C224140F,
	HPMixedRealityController__ctor_m9E53BEC36F0F78D75E3D75B1B0923B1B38F0374D,
	HandTrackingFeaturePlugin__ctor_m271EA4AB37E57E2F8A8C3E97AF0DFA2210D472CC,
	HoloLensFeaturePlugin__ctor_m46DE918A938970EF87CE6CCF3987275146403454,
	HoloLensFeaturePlugin_TryAcquireSceneCoordinateSystem_m3C10BA132FF399720A2E3BFC71AD61541BB7839C,
	HoloLensFeaturePlugin_TryAcquirePerceptionSpatialAnchor_mB750E3B92A2A80460104D812486F10140D70223F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FeatureUtils_ToTrackableId_m7A0DF91C9E58B6E7FD8D46A1857A817FE9655862,
	FeatureUtils_ToGuid_m65B1FC59E6FA5CEF419854060A90B7DE8F93A006,
	NativeLibTokenAttribute_get_NativeLibToken_m1E0D4133836DA7016A8CDC0D1C1FAA88E5B449CA,
	NativeLibTokenAttribute_set_NativeLibToken_mA68CB70DF5B14C74A69AD1E158B81F016B4B928F,
	NativeLibTokenAttribute__ctor_m3133A06535E087EF597350974028CB3B5047ED18,
	NativeLib_InitializeNativeLibToken_m81257382A6C069BCEA9827E59615EA1BD9DAD69D,
	NativeLib_HookGetInstanceProcAddr_mD4463C5A2273A46471E31CC473C7182766ACA679,
	NativeLib_GetInstanceProcAddr_m84620E93A762D8FBA8D0E1C048552CF45C8F583D,
	NativeLib_SetXrInstance_m83F1429AA035535FB64EE7A2043D20ED8B616390,
	NativeLib_SetXrSystemId_m012911CE48E4D46715E49671078E582CBD9CA5B4,
	NativeLib_SetXrSession_mBC371F3F4E6FB2E2EA346B0BF9C11A2AE3493302,
	NativeLib_SetXrSessionRunning_mD14F928350AB73622DD2701E54F0162849B4F4BF,
	NativeLib_SetSessionState_m752D96C4C84A682BEAAE33A1E94E9159D18976BB,
	NativeLib_SetSceneOriginSpace_mCD499B60B7F81481A9A522D4DC11560785462889,
	NativeLib_CreateSceneUnderstandingProvider_m24121E24AABDC8ED606D92A34BCD30D6325AEE0D,
	NativeLib_SetSceneUnderstandingActive_m950430F0454632292CEA18AC22752918D5105029,
	NativeLib_SetPlaneDetectionMode_m49EA191D8BCA0EFCACA42B43F7480616FE5EA3A0,
	NativeLib_GetNumPlaneChanges_m54B42731A229E4E6043444DF047BCEA9721D52DC,
	NativeLib_GetPlaneChanges_mDAB819014D611F2EFF5A1240847552AFF19AB4A0,
	NativeLib_CreateAnchorProvider_mD2094B903F5B800505134C035A52D9CEC3AA8331,
	NativeLib_TryAddAnchor_m4F5F1B79B8A82459BA41F3E9A31A287B45B75093,
	NativeLib_TryRemoveAnchor_m053AEA4308FB0237BE6FEE3C20F466C05EA09B16,
	NativeLib_GetNumAnchorChanges_m99E1399286E46F015FF40A6F2819BC9BFF1DEEF3,
	NativeLib_GetAnchorChanges_m549F1D7B4EF421FCAEE99184BD19C29752CF9E71,
	NativeLib_LoadAnchorStore_mB6B782526F92CA3D86028AE5495D6F3E25973819,
	NativeLib_GetNumPersistedAnchorNames_mA4438EB88A6B6841FDFD89F2A319279875169A44,
	NativeLib_GetPersistedAnchorName_mF25E5A0F7B926E2F4F4B591B19B092715EF04E4C,
	NativeLib_LoadPersistedAnchor_mCCCA9BAAF0607671A1E59B4742067EEC1F22CACA,
	NativeLib_TryPersistAnchor_m0DECB562CC141274520CCB246C7721D7C15F7C7C,
	NativeLib_UnpersistAnchor_m402B9AFA1DD1DD159C82D7EAFA2B9793054DF007,
	NativeLib_ClearPersistedAnchors_m80A84716788D0EE6F9F146FFC23A917F59909DC1,
	NativeLib_GetHandJointData_mBD6CABEA9ACE681D7F2AAF7E4EC075B6A365475B,
	NativeLib_TryLocateHandMesh_m44907E709723D0B06F5036D0799A11320AE24118,
	NativeLib_TryGetHandMesh_m3E1D62053260FAE972CA62016C78C2A42387428A,
	NativeLib_TryGetHandMeshBufferSizes_mC6128E4F2C30352D96E039A7CA070E2B15194925,
	NativeLib_TryEnableRemotingOverride_m29AEEFA1871D473D2745DB94D7399B8569B63593,
	NativeLib_ResetRemotingOverride_mC66AC58D7FD6E20216C01D70E6C991009025E89A,
	NativeLib_ConnectRemoting_mD0C56272108E10664423548F4FFAA4E4B63334B1,
	NativeLib_DisconnectRemoting_m5F2D5C74C51DF89C24B217D8FC5D465B5C66B22F,
	NativeLib_TryGetRemotingConnectionState_m1D38B9DE045B2FFAC3C09E7F705D6CEDC4EF3D0D,
	NativeLib_TryCreateSpaceFromStaticNodeId_m01A69A41D7E1A4018C2ECAF0ECE69A2E5205F784,
	NativeLib_TryLocateSpace_mAE1D17C66EFC2CF6454746B8F8C195FB96D402C1,
	NativeLib_TryAcquireSceneCoordinateSystem_mA3177E8BB3AD6B10E639BC175C39283A6BA918D3,
	NativeLib_TryAcquirePerceptionSpatialAnchor_m9B50DC084DD34CD699689AA7FA60AA0F92C33056,
	NativeLib__ctor_m41347442DBD619986F4E901D741DA1A41A35946C,
	OpenXRAnchorStoreFactory_LoadAnchorStoreAsync_m12D4957E452A9F50845E9ABCCF30F0847CA2DBFE,
	OpenXRAnchorStoreFactory__cctor_mCDDDC2539476B73F8557CA5565D20577415A3866,
	U3CU3Ec__cctor_m56329537EFC168C4C4ACE319028572DDCF30F640,
	U3CU3Ec__ctor_mC78C4A92A9D067E2B47A16A9FD257A858E4F7667,
	U3CU3Ec_U3CLoadAnchorStoreAsyncU3Eb__2_0_mE20122EC0C2727630F76DA3589B46EB94EE6DAB6,
	OpenXRAnchorStore__ctor_mEB085F594A7E919BE29DAB959D5C83FFCE14B704,
	OpenXRAnchorStore_get_PersistedAnchorNames_m4AAFD1C75A39A057709C557E3DEC1B48022BF745,
	OpenXRAnchorStore_UpdatePersistedAnchorNames_m4F69828BC42D615F2B4AD0F2E38932671979C132,
	OpenXRAnchorStore_LoadAnchor_m198AD532CA4648DB96993CB18D06AF2B894B5F0B,
	OpenXRAnchorStore_TryPersistAnchor_m12ECD1E0183D3F69F7B14E323F2B26EEC305539F,
	OpenXRAnchorStore_UnpersistAnchor_m5AB186165AFD489F117BC42A074B0F05FD2C14DE,
	OpenXRAnchorStore_Clear_mF9EAA9B0323A45C7A03655C9EFCDAE1187B51184,
	OpenXRAnchorStore__cctor_m6CD7165BF4EEBC0D3F75F08BC1A0DB38659A68F4,
	AnchorSubsystem_RegisterDescriptor_m05F13E3CC968276C9553A108272FB9A68A97D5F1,
	AnchorSubsystem__ctor_mF50564F70BBE697D9F5A4E2D5556CA4ABD5147F0,
	OpenXRProvider__ctor_m8DC96C043F6972BC08A2D1B653C7B0EA74F73782,
	OpenXRProvider_Destroy_m91435B6B58043B9093D454409FFCDC52D264E69D,
	OpenXRProvider_Start_m07D518C964D7CF7BDBD8515FDEE29E79F7188480,
	OpenXRProvider_Stop_mF8D132C1EB8EB78F38A8585D62B8408E87BBC665,
	OpenXRProvider_GetChanges_m514C0F27748A988E0DB0FE727A4F5541B168628E,
	OpenXRProvider_ToXRAnchor_m6F4AC3C2D981AFFCAACB0865FBD1EBAFFB80ED00,
	OpenXRProvider_TryAddAnchor_m8302C7C57F5D1D1CA341807033B43E89E397D9E0,
	OpenXRProvider_TryAttachAnchor_m52C56723C94868872BA1756ADFFE542E719C4A30,
	OpenXRProvider_TryRemoveAnchor_mB18B71C794BE19C9E8A252B8BC71FCED6B806ED0,
	AnchorSubsystemController__ctor_m50CFFC6B09DCA86FBD7FB569480EDC9EBED763FC,
	AnchorSubsystemController_OnSubsystemCreate_m5509284C4C71533DE139537DB2020852050EDEA9,
	AnchorSubsystemController_OnSubsystemStart_mB035923F3A90B46D42F2D9B402A59286EAFDDE2D,
	AnchorSubsystemController_OnSubsystemStop_m4CD2A808BF107D6A0E1FDBBA36E9AD4208E325EC,
	AnchorSubsystemController_OnSubsystemDestroy_m455A34ACC209D3A21D15EBAE97B315E4AF103DA0,
	AnchorSubsystemController__cctor_m15D0F86557975A471DAA760765E362C076152C02,
	HandTrackingSubsystemController__ctor_mC2B8C8D115CD4623282BEEF8E31B03CDB4BBC992,
	HandTrackingSubsystemController_OnSubsystemCreate_m8BFEB28514DD7F2265091D563DFC5FB6830CA9AF,
	HandTrackingSubsystemController_OnSubsystemStart_m8517B9E90CB1CD76989A5BF1DC0E3FFCBACFD8D4,
	HandTrackingSubsystemController_OnSubsystemStop_mFA520313379300F894FA201B85D4182F97EB5FF7,
	HandTrackingSubsystemController_OnSubsystemDestroy_m0A083F0821591236239A80400A549614FA53D9B3,
	MeshSubsystemController__ctor_m18E6586F3FA5137B6FB112E493226698E2CAA876,
	MeshSubsystemController_OnSubsystemCreate_m67776BA8F73289CFEC5D4F2D4EA3BC840F1BB144,
	MeshSubsystemController_OnSubsystemDestroy_m193625592355574009F00261701A35153F7CDDA3,
	MeshSubsystemController__cctor_m9115749A6A8A4503E8B4816C34D751BB42EFDA4B,
	PlaneSubsystem_RegisterDescriptor_m7F301C3061BA767C73C3841743EAAF0F67694FF4,
	PlaneSubsystem__ctor_m92D726C6B59917D31864546D4C724426A370801D,
	OpenXRProvider__ctor_m0810D0FBB2D3DA67331CC85048163C92CD79223D,
	OpenXRProvider_Start_m9A67B2444C4C054B680554E57476905D29B9057A,
	OpenXRProvider_Stop_m52979099FC5DFFCB69BE529886255CB125B9B592,
	OpenXRProvider_Destroy_m6A4F9A31143AB6758888C67998EC809E5AE9C71D,
	OpenXRProvider_get_currentPlaneDetectionMode_m5112A4030805746A3D8B1AF2804E4DCDBE466F40,
	OpenXRProvider_get_requestedPlaneDetectionMode_mED9D9BFC796B50B9D0981630C77842987272751E,
	OpenXRProvider_set_requestedPlaneDetectionMode_mC5FEA58C621235A65724016846BA9EA956650732,
	OpenXRProvider_GetChanges_mF6B8282479E9F42AA40158953E617F599280E10C,
	OpenXRProvider_ToPlaneClassification_m5013D8712913D88E1B8D08ACA9F4E28879FDE31D,
	OpenXRProvider_ToBoundedPlane_m4E4B463FC8D48BE21F61ACC518BB702912B55BE3,
	PlaneSubsystemController__ctor_mDC113D59676EFA45A11F486B2A82FE44ECF6E744,
	PlaneSubsystemController_OnSubsystemCreate_m2E658AB785FDF4C7CADC888C89593462BB34BA68,
	PlaneSubsystemController_OnSubsystemDestroy_m0C3A0036BE5A59BEC8294643292084E69D10A32D,
	PlaneSubsystemController__cctor_m46A7EE7A7921D686BF5D536F6994513CF09877C3,
	RaycastSubsystem_RegisterDescriptor_m9C781123128B093724F4EE7BD6CC91D962F398F3,
	RaycastSubsystem__ctor_mE9A9676881F16093A75F8EA0F3F3C144C934E78C,
	OpenXRProvider_TryAddRaycast_mAB13C8024EC6BD339D8CD770307311B0A5B2EEA0,
	OpenXRProvider_TryAddRaycast_m9758DFBF8D6F4837B16F02F0D96C8E2059257A05,
	OpenXRProvider_RemoveRaycast_m64C3FB8BD3C61E0D2FFAEE6A46F4CFAED0326B01,
	OpenXRProvider__ctor_mABC309D451B3863AC74398410EB1CA0EBC5FFA83,
	RaycastSubsystemController__ctor_m18B861B52A60AB845CC3D7A4F40B4C263A65EB68,
	RaycastSubsystemController_OnSubsystemCreate_m7AB05789E754DD2864B9A1DFCE5C6CACD82C68E7,
	RaycastSubsystemController_OnSubsystemDestroy_m397DD6C9E32AC9CFE4407F949E1851C1A8E03581,
	RaycastSubsystemController__cctor_m5FC43F03D5C5D6E88E3E12225EDAFB3B0E64CFAB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SubsystemController__ctor_m15FB20C0E85F5CDF70282C8908100C8A7CD4ED4A,
	SubsystemController_OnSubsystemCreate_m46A1F68EA94FF1B417B336B87A51E923F6C6B77E,
	SubsystemController_OnSubsystemStart_mDC204F22C801970C9338E443D8CC74687DC465DD,
	SubsystemController_OnSubsystemStop_mFC6CE5C9E2D60F6B6FED51D5109B45DE08D96FCE,
	SubsystemController_OnSubsystemDestroy_mBCE41C6C7A0382BC11F39026909517A7612E09AD,
	HandTracker__ctor_mFC002E1312D55C5EDD3D10C9B4C238F73CF0C5B0,
	HandTracker_TryLocateHandJoints_m8BD920DD5BE76143129A3859D17E5D2AA2C6989D,
	HandJointLocation_get_PoseFlags_m59BFC0160D28158C2C5E94643054886705832873,
	HandJointLocation_get_Rotation_mA49A45860B0B0CA1AF450DCA3F6593DDAA941946,
	HandJointLocation_get_Position_mC02E65EBA165EF118F6A9C99A47F94790F0225EB,
	HandJointLocation_get_Radius_m593CA1C90F41132D1E3569965C9B53C73E29071F,
	HandJointLocation__ctor_m60ED7D572A93EF4F57208983FB3638E03B6E3F3A,
	AppRemoting_get_Configuration_m986F5261E0B5DF5918670E4BDB303DD6BB6CE6BB,
	AppRemoting_set_Configuration_m330236A0A1624400D8C2B79055275A5DBCDDD080,
	AppRemoting_Connect_mFB5585D81BEF6162D314684C0C1BD5D92EEFA435,
	AppRemoting_Disconnect_mAE0A81B850728822A9DEE75A00C297CCC906549D,
	AppRemoting_TryGetConnectionState_mD917542223797465AA54EA39811C4910212348C3,
	U3CConnectU3Ed__4__ctor_mE84343A49B0B3DC1D602D771D3E878C7DD5A06A8,
	U3CConnectU3Ed__4_System_IDisposable_Dispose_m5B1C77D83F06076D97CA121CF6E69BF5E5EDFD31,
	U3CConnectU3Ed__4_MoveNext_m1A8964EC7416B44B2AFE8F013DF193FACA85F273,
	U3CConnectU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBA0A91330F728EBA406ED19CB72A80CA118C0EF9,
	U3CConnectU3Ed__4_System_Collections_IEnumerator_Reset_mEF91BB0D15F59540613C5DA826BA48CACCA2D8A5,
	U3CConnectU3Ed__4_System_Collections_IEnumerator_get_Current_m9CC5E737A39AABD0493115AF1F3D1614D9B2B5C3,
	AppRemotingPlugin_HookGetInstanceProcAddr_m31BB148E2D2D2A77FC2E6FC7C8CF2B91F070BD4C,
	AppRemotingPlugin_OnInstanceCreate_mF97A486535A5E5F405DF63F7888E91365D52487E,
	AppRemotingPlugin_OnInstanceDestroy_mD14D534C2F5077AA616D709C4405845E8DA0D5A1,
	AppRemotingPlugin_OnSystemChange_m6F340CB1DCE89999073AD8C3E0E904964AF9B4EA,
	AppRemotingPlugin_OnSessionStateChange_m5822F18A4E025121C9758DACD16389310CCF2C74,
	AppRemotingPlugin__ctor_m4C9DD14B55D812BCB1F43656660647DD6FC4AAEF,
	U3CU3Ec__cctor_m9EB91B1FAF58CD4567692E52F701DCF00CFEA761,
	U3CU3Ec__ctor_m914051A6AAEFA1757292D09D57152A3EA4B0C420,
	U3CU3Ec_U3COnSystemChangeU3Eb__8_0_m2EEB89E0083203E1265A13BDD49C2A0DEEA4CCA5,
	EditorRemotingPlugin_HookGetInstanceProcAddr_mBC57B868CED9BE657F9409ECC892A87149E7EA39,
	EditorRemotingPlugin_OnInstanceCreate_mA85E46C417E846CF247097FE2935CC41377CE8F6,
	EditorRemotingPlugin_OnInstanceDestroy_m70C271FE49E987000476C490C1BDB5B86D19142A,
	EditorRemotingPlugin_OnSystemChange_m377B9FA110A7814B34F23D563574DF9BDFA638E7,
	EditorRemotingPlugin_OnSessionStateChange_mB2369C3B0913F984762627E941578F807F021CEE,
	EditorRemotingPlugin_HasValidSettings_m6505DB3B86D285EE886805B6DDFD9BD0BFED8C02,
	EditorRemotingPlugin__ctor_mB9F1507DF72CDDF13B33531FFBB2C3C0C9B70D43,
	EditorRemotingPlugin_U3COnSystemChangeU3Eb__13_0_m476B42C14BD5D134898232591CEDCC6E41A629EE,
	ARAnchorExtensions_GetOpenXRHandle_m62E3251ED977F25F3F3FCF2F154582ADFF01DE84,
	AnchorManagerExtensions_LoadAnchorStoreAsync_mCF939DB55C69215007B47E334D92613B1B2864E0,
	XRAnchorExtensions_GetOpenXRHandle_mA35F8245608C8F50704657EC1A5CA8084FADE65B,
	AnchorSubsystemExtensions_LoadAnchorStoreAsync_mFEF6B387788DE8DF416392F252252C19DD0C0367,
};
extern void U3CLoadAsyncU3Ed__0_MoveNext_mD07CA3DEB02D0D731328F87FAEEBDE336B391476_AdjustorThunk (void);
extern void U3CLoadAsyncU3Ed__0_SetStateMachine_m02FDE1D534549B190ABD2C8C23D1AE181F847620_AdjustorThunk (void);
extern void HandJointLocation_get_IsTracked_m51143EF40DA6C6DED3F7F373D8DD95D1481536AC_AdjustorThunk (void);
extern void HandJointLocation_get_Pose_m2C026225983AF66BBFF803DA3B629AA60D7E7111_AdjustorThunk (void);
extern void HandJointLocation_get_Radius_mC4680C1AB3CA80BB367A31F313506FD55634B07E_AdjustorThunk (void);
extern void U3CLoadAsyncU3Ed__6_MoveNext_mBCF49E09B452975FEDE05040DA5B235970426A29_AdjustorThunk (void);
extern void U3CLoadAsyncU3Ed__6_SetStateMachine_m534F081E0324AE9975FFCD32E12481754FBC0273_AdjustorThunk (void);
extern void HandJointLocation_get_PoseFlags_m59BFC0160D28158C2C5E94643054886705832873_AdjustorThunk (void);
extern void HandJointLocation_get_Rotation_mA49A45860B0B0CA1AF450DCA3F6593DDAA941946_AdjustorThunk (void);
extern void HandJointLocation_get_Position_mC02E65EBA165EF118F6A9C99A47F94790F0225EB_AdjustorThunk (void);
extern void HandJointLocation_get_Radius_m593CA1C90F41132D1E3569965C9B53C73E29071F_AdjustorThunk (void);
extern void HandJointLocation__ctor_m60ED7D572A93EF4F57208983FB3638E03B6E3F3A_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[12] = 
{
	{ 0x06000005, U3CLoadAsyncU3Ed__0_MoveNext_mD07CA3DEB02D0D731328F87FAEEBDE336B391476_AdjustorThunk },
	{ 0x06000006, U3CLoadAsyncU3Ed__0_SetStateMachine_m02FDE1D534549B190ABD2C8C23D1AE181F847620_AdjustorThunk },
	{ 0x0600001A, HandJointLocation_get_IsTracked_m51143EF40DA6C6DED3F7F373D8DD95D1481536AC_AdjustorThunk },
	{ 0x0600001B, HandJointLocation_get_Pose_m2C026225983AF66BBFF803DA3B629AA60D7E7111_AdjustorThunk },
	{ 0x0600001C, HandJointLocation_get_Radius_mC4680C1AB3CA80BB367A31F313506FD55634B07E_AdjustorThunk },
	{ 0x06000032, U3CLoadAsyncU3Ed__6_MoveNext_mBCF49E09B452975FEDE05040DA5B235970426A29_AdjustorThunk },
	{ 0x06000033, U3CLoadAsyncU3Ed__6_SetStateMachine_m534F081E0324AE9975FFCD32E12481754FBC0273_AdjustorThunk },
	{ 0x06000106, HandJointLocation_get_PoseFlags_m59BFC0160D28158C2C5E94643054886705832873_AdjustorThunk },
	{ 0x06000107, HandJointLocation_get_Rotation_mA49A45860B0B0CA1AF450DCA3F6593DDAA941946_AdjustorThunk },
	{ 0x06000108, HandJointLocation_get_Position_mC02E65EBA165EF118F6A9C99A47F94790F0225EB_AdjustorThunk },
	{ 0x06000109, HandJointLocation_get_Radius_m593CA1C90F41132D1E3569965C9B53C73E29071F_AdjustorThunk },
	{ 0x0600010A, HandJointLocation__ctor_m60ED7D572A93EF4F57208983FB3638E03B6E3F3A_AdjustorThunk },
};
static const int32_t s_InvokerIndices[298] = 
{
	6935,
	6935,
	3982,
	1725,
	4910,
	3982,
	6850,
	6933,
	4910,
	4910,
	3982,
	4910,
	7060,
	4910,
	7157,
	7157,
	3948,
	1048,
	1058,
	7190,
	7157,
	7157,
	3948,
	1697,
	7190,
	4863,
	4836,
	4870,
	7157,
	4791,
	4791,
	4791,
	4791,
	2955,
	4910,
	7190,
	6936,
	6928,
	4760,
	3918,
	1691,
	4910,
	4825,
	3650,
	1751,
	3982,
	4910,
	6935,
	3982,
	4910,
	3982,
	4910,
	4910,
	4910,
	4910,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4825,
	3982,
	4910,
	4910,
	4910,
	4910,
	2956,
	2953,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	7028,
	6795,
	4791,
	3949,
	4910,
	7056,
	6249,
	5850,
	6569,
	6569,
	6569,
	6572,
	6568,
	6569,
	7056,
	6572,
	6568,
	5411,
	5744,
	7056,
	5341,
	6402,
	5411,
	5744,
	7056,
	6829,
	5745,
	6188,
	5937,
	6570,
	7056,
	5674,
	5340,
	5031,
	5933,
	6989,
	7056,
	6571,
	7056,
	5933,
	5934,
	5675,
	6250,
	6248,
	4910,
	6935,
	7190,
	7190,
	4910,
	4825,
	4910,
	4825,
	4910,
	3650,
	1725,
	3982,
	4910,
	7190,
	7190,
	4910,
	4910,
	4910,
	4910,
	4910,
	1294,
	4165,
	1728,
	1109,
	3485,
	2223,
	3982,
	3982,
	3982,
	3982,
	7190,
	3982,
	3982,
	3982,
	3982,
	3982,
	2223,
	3982,
	3982,
	7190,
	7190,
	4910,
	4910,
	4910,
	4910,
	4910,
	4790,
	4790,
	3948,
	1293,
	2763,
	1306,
	2223,
	3982,
	3982,
	7190,
	7190,
	4910,
	1111,
	1099,
	4043,
	4910,
	2223,
	3982,
	3982,
	7190,
	4791,
	4791,
	4791,
	4790,
	4791,
	4863,
	-1,
	-1,
	-1,
	-1,
	3982,
	3982,
	3982,
	3982,
	3982,
	2048,
	1697,
	4791,
	4840,
	4905,
	4870,
	3921,
	7175,
	7062,
	6938,
	7190,
	6337,
	3948,
	4910,
	4863,
	4825,
	4910,
	4825,
	2954,
	3386,
	3949,
	3949,
	2048,
	4910,
	7190,
	4910,
	4910,
	2954,
	3386,
	3949,
	3949,
	2048,
	4863,
	4910,
	4910,
	6851,
	6935,
	6856,
	6935,
};
static const Il2CppTokenRangePair s_rgctxIndices[5] = 
{
	{ 0x02000017, { 0, 14 } },
	{ 0x0600007F, { 14, 1 } },
	{ 0x06000080, { 15, 1 } },
	{ 0x06000081, { 16, 1 } },
	{ 0x06000082, { 17, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[18] = 
{
	{ (Il2CppRGCTXDataType)3, 35825 },
	{ (Il2CppRGCTXDataType)1, 729 },
	{ (Il2CppRGCTXDataType)2, 8048 },
	{ (Il2CppRGCTXDataType)3, 35820 },
	{ (Il2CppRGCTXDataType)3, 35822 },
	{ (Il2CppRGCTXDataType)3, 35823 },
	{ (Il2CppRGCTXDataType)3, 35821 },
	{ (Il2CppRGCTXDataType)3, 35826 },
	{ (Il2CppRGCTXDataType)3, 35824 },
	{ (Il2CppRGCTXDataType)3, 35827 },
	{ (Il2CppRGCTXDataType)3, 35831 },
	{ (Il2CppRGCTXDataType)3, 35829 },
	{ (Il2CppRGCTXDataType)3, 35830 },
	{ (Il2CppRGCTXDataType)3, 35828 },
	{ (Il2CppRGCTXDataType)3, 51314 },
	{ (Il2CppRGCTXDataType)3, 51318 },
	{ (Il2CppRGCTXDataType)3, 51320 },
	{ (Il2CppRGCTXDataType)3, 51316 },
};
extern const CustomAttributesCacheGenerator g_Microsoft_MixedReality_OpenXR_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Microsoft_MixedReality_OpenXR_CodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_OpenXR_CodeGenModule = 
{
	"Microsoft.MixedReality.OpenXR.dll",
	298,
	s_methodPointers,
	12,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	5,
	s_rgctxIndices,
	18,
	s_rgctxValues,
	NULL,
	g_Microsoft_MixedReality_OpenXR_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
